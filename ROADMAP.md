# Garta Roadmap
This is an open-ended development roadmap which is subject to change anytime. See the [change log] for more information about the past changes.

## Version 0.2
- attractions (a.k.a. geo-bookmarks) **[90%]**
- attraction types **[20%]**
- context menu **[100%]**
- zooming with keyboard **[100%]**
- units of measurement (nautical, metric, imperial) **[100%]**
- full data persistence **[40%]**
- undo/redo **[100%]**
- https support **[100%]**
- simple configure, installer and uninstaller scripts **[100%]**
- application icon and .desktop file **[100%]**
- firejail profile **[100%]**
- MapCanvas::draw refactored (for better modularity and maintainability) **[100%]**

## Version 0.3
- refactoring network code by replacing hyper with [reqwest]
- center the initial location based on system time zone offset
- layers dialog
- transparent map layers
- element color based on time interval (like from mid-May to August)

## Version 0.4
- waypoints
- route planning
- vehicles **[20%]**
- integration with rust-gpx ?
- GPX export
- current time controls (MapView)
- distance tool

## Version 0.5
- GPX import
- KML import
- track editing
- track rendering
- track statistics
- track section VMG

## Version 0.6
- system clipboard
- drag & drop
- keyboard shortcuts
- scrolling with keyboard
- GeoURI support
- final application ID
- zoom in/out buttons on map canvas
- text widget undo/redo support

## Version 0.7
- areas/polygons/circles
- geocoord module relicensed and moved to a separate repository and also published at crates.io
- KML export

## Version 0.8
- maps dialog
- OpenGIS WMTS standard support
- HiDPI tile support (GDK scale factor)

## Version 0.9
- internationalization, [Fluent] (contributors needed)
- man page

## Version 0.10
- vehicles dialog
- track replay
- fullscreen mode

## Version 0.11
- locations search by name, and other possible meta queries
- settings dialog
- settings persistence **[20%]**
- sunrise/sunset awareness
- time zones with [chrono-tz]

## Version 0.12
- real-time location support with [gpsd]
- position on route and ETA

## Version 0.13
- TODO and FIXME sections finished
- polished error handling and code in general
- enhanced input validation

## Version 1.0
- stable file formats and directory structure
- debugging removed from stable parts of the code

## Sooner or later
- GTK4 migration (if supported by gtk-rs), or alternatively refactoring for [GTK4 forward compatibility]
- GeoRSS, GeoJSON and GeoTagging
- collaborative layers editing
- vector tile maps
- SVG and PNG export
- printing support
- track import from sports web sites
- globe-wide offline maps
- [scripting language support]
- AIS support (NMEA/TCP), including real-time location

## Non-Goals
- OpenStreetMap data editing (there is [JOSM] for that)
- street-based routing – there are enough of both prorietary and libre options available
- no dependency on [geo](https://github.com/georust/rust-geo) crate as whole because it doesn't meet the non-Euclidean geometry needs of Garta

[change log]: CHANGELOG.md
[gpsd]: http://catb.org/gpsd/
[scripting language support]: https://github.com/rust-unofficial/awesome-rust#scripting
[JOSM]: https://josm.openstreetmap.de
[chrono-tz]: https://crates.io/crates/chrono-tz
[reqwest]: https://crates.io/crates/reqwest
[GTK4 forward compatibility]: https://developer.gnome.org/gtk4/unstable/gtk-migrating-3-to-4.html
[Fluent]: https://projectfluent.org/

