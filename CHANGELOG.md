# Change Log
All notable changes to Garta project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## Unreleased
### Added
- attractions
- context menu
- map cycling with TAB key
- zoom in/out with keyboard
- https support ([hyper-rustls])
- passing command line arguments to GTK 
- application icon file
- configure, install and uninstall scripts
### Fixed
- coordinates update bug when scrolling

## 0.1 - 2017-02-08
### Added
- smooth zooming, scrolling and cursor support to map canvas
- maps, layers (hidden), coordinates and zoom level widgets to status bar
- map canvas view persistence
- required copyright texts with clickable urls
- tile cache with multi-threaded background tile loading
- map metadata loading from JSON files
- logging (env_logger)

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/
[hyper-rustls]: https://github.com/ctz/hyper-rustls

