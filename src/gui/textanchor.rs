// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use geocoord::{Vector};

/// Instruction on how text should be placed related to the element origin. 
/// Margin is the pixel offset between the container and the element origin.
#[derive(Copy, Clone, Debug)]
pub enum TextAnchor {
    Center, 
    North{margin: f64}, 
    NorthEast{margin: f64}, 
    East{margin: f64}, 
    SouthEast{margin: f64}, 
    South{margin: f64}, 
    SouthWest{margin: f64}, 
    West{margin: f64}, 
    NorthWest{margin: f64},
}

impl TextAnchor {
    /// Make offset for the given text width and heights. The result is relative to the center.
    ///
    /// * `tw` - text width
    ///
    /// * `th` - text height
    ///
    pub fn make_offset(&self, tw: f64, th: f64) -> Vector {
        match self {
            TextAnchor::Center => {
                Vector::zero()
            },
            TextAnchor::North{margin} => {
                Vector::new(0.0, -(margin + th / 2.0))
            }
            TextAnchor::NorthEast{margin} => {
                Vector::new(*margin + tw / 2.0, -(margin + th / 2.0))
            }
            TextAnchor::East{margin} => {
                Vector::new(*margin + tw / 2.0, 0.0)
            }
            TextAnchor::SouthEast{margin} => {
                Vector::new(*margin + tw / 2.0, margin + th / 2.0)
            }
            TextAnchor::South{margin} => {
                Vector::new(0.0, *margin + th / 2.0)
            }
            TextAnchor::SouthWest{margin} => {
                Vector::new(-(*margin + tw / 2.0), margin + th / 2.0)
            }
            TextAnchor::West{margin} => {
                Vector::new(-(*margin + tw / 2.0), 0.0)
            }
            TextAnchor::NorthWest{margin} => {
                Vector::new(-(*margin + tw / 2.0), -(margin + th / 2.0))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_text_anchor() {
        let v = TextAnchor::Center.make_offset(80.0, 40.0);
        assert_eq!(v.x, 0.0);
        assert_eq!(v.y, 0.0);
        let v = TextAnchor::North{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, 0.0);
        assert_eq!(v.y, -30.0);
        let v = TextAnchor::NorthEast{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, 50.0);
        assert_eq!(v.y, -30.0);
        let v = TextAnchor::East{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, 50.0);
        assert_eq!(v.y, 0.0);
        let v = TextAnchor::SouthEast{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, 50.0);
        assert_eq!(v.y, 30.0);
        let v = TextAnchor::South{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, 0.0);
        assert_eq!(v.y, 30.0);
        let v = TextAnchor::SouthWest{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, -50.0);
        assert_eq!(v.y, 30.0);
        let v = TextAnchor::West{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, -50.0);
        assert_eq!(v.y, 0.0);
        let v = TextAnchor::NorthWest{margin: 10.0}.make_offset(80.0, 40.0);
        assert_eq!(v.x, -50.0);
        assert_eq!(v.y, -30.0);
    }
}

