// Garta - A geography application for GTK3
// Copyright (C) 2016-2017, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate gtk;
extern crate gio;
extern crate gdk;
extern crate gdk_pixbuf;
extern crate glib;
extern crate cairo;
extern crate reqwest;
//use std::future::Future;
use self::gtk::ImageExt;

//use std::fmt;

use self::gdk::prelude::{GdkContextExt};
use self::cairo::{Format, ImageSurface};
use self::gdk_pixbuf::{Pixbuf};

use geocoord::geo::{Vector};

/// Sprite raster lists the supported representations of the bitmap.
enum SpriteRaster {
    ImageSurface {
        /// Surface for the pixel data.
        surface: ImageSurface,

        /// Sprite width in pixels.
        width: i32,
        
        /// Sprite height in pixels.
        height: i32,
    },
    Pixbuf {
        pixbuf: Pixbuf,
    },
}

/// An offline bitmap which ban be drawn to/with Cairo Context.
pub struct Sprite {
    /// Bitmap
    raster: SpriteRaster,

    /// Offset of the sprite. The components are typically less than the tile dimensions.
    pub offset: Vector,
    
    /// Zoom level, if applicable.
    pub zoom_level: Option<u8>,

    /// Source of the sprite, typically an uri.
    pub source: Option<String>,
}

impl Sprite {
    /// Constructor with most of the parameters.
    pub fn new(width: i32, height: i32, zoom_level: Option<u8>, transparent: bool) -> Result<Sprite, cairo::Status> {
        Sprite::with_offset(width, height, Vector::zero(), zoom_level, transparent)
    }

    /// Construct a transparent sprite with width and height only.
    pub fn with_size_and_transparency(width: i32, height: i32) -> Result<Sprite, cairo::Status> {
        Sprite::with_offset(width, height, Vector::zero(), None, true)
    }
    
    /// Constructor with most of the parameters, including offset.
    pub fn with_offset(width: i32, height: i32, offset: Vector, 
                       zoom_level: Option<u8>, transparent: bool) 
                      -> Result<Sprite, cairo::Status> 
        {
        Result::Ok(Sprite {
            raster: SpriteRaster::ImageSurface {
                surface: {
                    if transparent {
                        ImageSurface::create(Format::ARgb32, width, height)?
                    } else {
                        ImageSurface::create(Format::Rgb24, width, height)?
                    }
                },
                width: width,
                height: height,
            },
            offset: offset,
            zoom_level: zoom_level,
            source: None,
        })
    }
    
    /// Constructs a sprite from a pixbuf.
    fn with_pixbuf(pixbuf: Pixbuf, source: Option<String>) -> Result<Sprite, cairo::Status> {
        Result::Ok(Sprite {
            raster: SpriteRaster::Pixbuf {
                pixbuf: pixbuf,
            },
            offset: Vector::zero(),
            zoom_level: None,
            source: source,
        })
    }

    // Getter for width
    pub fn width(&self) -> i32 {
        match self.raster {
            SpriteRaster::ImageSurface { ref surface, width, height } => {
                width
            },
            SpriteRaster::Pixbuf { ref pixbuf } => {
                pixbuf.get_width()
            }
        }
    }

    // Getter for height
    pub fn height(&self) -> i32 {
        match self.raster {
            SpriteRaster::ImageSurface { ref surface, width, height } => {
                height
            },
            SpriteRaster::Pixbuf { ref pixbuf } => {
                pixbuf.get_height()
            }
        }
    }

    /// Start drawing on the sprite
    pub fn to_context(&mut self) -> cairo::Context {
        match self.raster {
            SpriteRaster::ImageSurface { ref surface, width, height } => {
                cairo::Context::new(&surface)
            },
            _ => {
                panic!("No surface!"); // TODO: convert
            }
        }
    }
    
    /// Makes the internal implementation read-only. Re-drawing requires
    /// internal unbaking. Currently this method does nothing.
    pub fn bake(&mut self) {
    }

    /// Paint this sprite on the given Cairo Context.
    pub fn paint_on(&self, c: &cairo::Context, x: f64, y: f64) {
        match self.raster {
            SpriteRaster::ImageSurface { ref surface, width, height } => {
                c.set_source_surface(&surface, x, y);
                c.paint();
            },
            SpriteRaster::Pixbuf { ref pixbuf } => {
                c.set_source_pixbuf(&pixbuf, x, y);
                c.paint();
            },
        }
    }

    /// Apply the bitmap to gtk::Image.
    pub fn apply_on(&self, image: &gtk::Image) {
        match self.raster {
            SpriteRaster::Pixbuf { ref pixbuf } => {
                image.set_from_pixbuf(Some(pixbuf));
            },
            _ => {
                panic!("No pixbuf!"); // TODO: convert
            }
        }
    }

}

impl Clone for Sprite {
    /// Deep clone of the data.
    fn clone(&self) -> Sprite {
        match self.raster {
            SpriteRaster::ImageSurface { ref surface, width, height } => {
                // Create a new surface with matching dimensions
                match ImageSurface::create(Format::Rgb24, width, height) {
                    Ok(new_surface) => {
                        // Copy surface contents
                        let c = cairo::Context::new(&new_surface);
                        c.set_source_surface(&surface, 0.0, 0.0);
                        c.paint();

                        // Return a new sprite
                        Sprite {
                            raster: SpriteRaster::ImageSurface {
                                surface: new_surface,
                                width: width,
                                height: height,
                            },
                            offset: self.offset,
                            zoom_level: self.zoom_level,
                            source: self.source.clone(),
                        }
                    },
                    Err(e) => {
                        panic!("Can't create a new ImageSurface: {:?}", e) // TODO: something less dramatic
                    }
                }
            },
            SpriteRaster::Pixbuf { ref pixbuf } => {
                Sprite {
                    raster: SpriteRaster::Pixbuf {
                        pixbuf: pixbuf.clone(), // TODO: is this a deep clone? pixbuf.copy()?
                    },
                    offset: self.offset,
                    zoom_level: self.zoom_level,
                    source: self.source.clone(),
                }
            }
        }
    }
}

pub fn fetch_sprite() {
}

/*
/// Loads a sprite either from network or file system asynchronously.
pub fn fetch_sprite(uri: &String) -> impl Future<Output = Result<&mut Sprite, fmt::Error>> {

    let response = reqwest::Client::new().get(uri.as_str()).send().await?;

}
*/

/*    
/// Loads a sprite either from network or file system asynchronously.
pub fn fetch_sprite<F>(uri: &String, callback: F) where F: FnMut(Result<&mut Sprite, fmt::Error>) {
    debug!("fetch_image({})", uri);

    let response = reqwest::r#async::Client::new()
        .get(uri.as_str())
        .send()
        .map(|res| {
            if res.status() != reqwest::StatusCode::OK {
                warn!("Image request: {:?}", res.status());
                return;
            }
            
            if !res.headers().contains_key("Content-Type") {
                warn!("Image request: Missing content type header");
                return;
            }
            
            let content_type = res.headers()["Content-Type"].to_str().unwrap_or("");
            if !content_type.starts_with("image/") {
                warn!("Image request: Unexpected content type: {}", content_type);
                return;
            }

            debug!("fetch_image: TODO");
            //let pixbuf = Pixbuf::new_from_stream_async(); 
            // pub fn new_from_stream_async<'a, P: IsA<gio::InputStream>, Q: IsA<gio::Cancellable>, R: FnOnce(Result<Pixbuf, Error>) + Send + 'static>(stream: &P, cancellable: Option<&Q>, callback: R) {
            //let sprite = Sprite::with_pixbuf(pixbuf, Some(uri.clone()));
        });
    // TODO        
}
*/    

