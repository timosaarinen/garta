// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate lazy_static;

use std::sync::{RwLock};

use core::*;
use geocoord::{Location};

lazy_static! {
    static ref CLIPBOARD: RwLock<Clipboard> = { RwLock::new(Clipboard::new()) };
}

// ---- Clipboard ---------------------------------------------------------------------------------

// TODO: connection to the actual system clipboard

/// Wraps desktop environment clipboard.
pub struct Clipboard {
    item: Option<Item>,
}

impl Clipboard {
    /// The default constructor.
    fn new() -> Clipboard {
        Clipboard {
            item: None,
        }
    }

}

/// True if there is something on the clipboard that can be pasted.
pub fn is_some() -> bool {
    match CLIPBOARD.read() {
        Ok(c) => {
            if let Some(ref item) = c.item {
                match item {
                    Item::Coordinates{location} => {
                        false
                    },
                    _ => {
                        true
                    }
                }
            } else {
                false
            }
        },
        Err(e) => {
            warn!("failed to get clipboard from RwLock: {}", e);
            false
        }
    }
}

/// Copy an item from clipboard, if available.
pub fn get() -> Option<Item> {
    match CLIPBOARD.read() {
        Ok(c) => {
            c.item.clone()
        },
        Err(e) => {
            warn!("failed to get clipboard from RwLock: {}", e);
            None
        }
    }
}

/// Put an item to clipboard.
pub fn put(item: Item) {
    match CLIPBOARD.write() {
        Ok(mut c) => {
            c.item = Some(item);
        },
        Err(e) => {
            warn!("failed to get clipboard from RwLock: {}", e);
        }
    }
}


// ---- ClipboardItem -----------------------------------------------------------------------------

#[derive(Clone)]
pub enum Item {
    Coordinates{
        location: Location,
    },
    Attraction{
        attraction: Attraction,
    }
}

