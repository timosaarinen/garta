// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate gtk;
use self::gtk::prelude::*;

use std::rc::{Rc};

use core::{Hotspot, Command};
use geocoord::{Vector};

use gui::mapwindow::{MapWindow};
use gui::mapcanvas::{CoordinateContext};
use gui::attractiondialog::*;
use gui::clipboard;

pub fn populate_and_show_context_menu(context_menu: &mut gtk::Menu, event: &gdk::EventButton, 
                                      hotspot: Option<Hotspot>, map_win: Rc<MapWindow>,
                                      cc0: &mut CoordinateContext) {
    let wpos = Vector::with_tuple(event.get_position());

    // Remove existing menu items.
    for child in context_menu.get_children() {
        context_menu.remove(&child);
    }

    // MenuItem for Undo
    let item = gtk::MenuItem::new_with_label("Undo");
    if let Some(cmd) = map_win.atlas.borrow().peek_undo() {
        item.set_label(format!("{} ({})", "Undo", cmd.name()).as_str());
    } else {
        item.set_sensitive(false);
    }
    let mwin = map_win.clone();
    item.connect_activate(move |_| {
        debug!("undo action!");
        mwin.atlas.borrow_mut().undo();
        mwin.update_map();
    });
    context_menu.append(&item);
    
    // MenuItem for Redo
    let item = gtk::MenuItem::new_with_label("Redo");
    if let Some(cmd) = map_win.atlas.borrow().peek_redo() {
        item.set_label(format!("{} ({})", "Redo", cmd.name()).as_str());
    } else {
        item.set_sensitive(false);
    }
    let mwin = map_win.clone();
    item.connect_activate(move |_| {
        debug!("redo action!");
        mwin.atlas.borrow_mut().redo();
        mwin.update_map();
    });
    context_menu.append(&item);

    // Separator
    let item = gtk::SeparatorMenuItem::new();
    context_menu.append(&item);

    // MenuItem for Cut
    let cut_item = gtk::MenuItem::new_with_label("Cut");
    context_menu.append(&cut_item);

    // MenuItem for Copy
    let copy_item = gtk::MenuItem::new_with_label("Copy");
    context_menu.append(&copy_item);

    // MenuItem for Paste
    let paste_item = gtk::MenuItem::new_with_label("Paste");
    context_menu.append(&paste_item);

    // Separator
    let item = gtk::SeparatorMenuItem::new();
    context_menu.append(&item);

    // MenuItem for Delete
    let delete_item = gtk::MenuItem::new_with_label("Delete");
    context_menu.append(&delete_item);

    // Separator
    let item = gtk::SeparatorMenuItem::new();
    context_menu.append(&item);

    // MenuItem for Center
    let center_item = gtk::MenuItem::new_with_label("Center");
    let mwin = map_win.clone();
    let cc = cc0.clone();
    center_item.connect_activate(move |_| {
        debug!("center action!");
        mwin.map_view.borrow_mut().center = cc.clone().wpos_to_loc(wpos);
        mwin.redraw_map();
    });
    context_menu.append(&center_item);

    // MenuItem for Properties
    let properties_item = gtk::MenuItem::new_with_label("Properties");
    context_menu.append(&properties_item);

    if let Some(hotspot_) = hotspot {
        paste_item.set_sensitive(false);
        
        // Menu for attraction
        if let Some(attr) = map_win.atlas.borrow_mut().attractions.get(&hotspot_.id) {
            // Handler for Edit
            let hotspot = hotspot_.clone();
            let mwin = map_win.clone();
            properties_item.connect_activate(move |_| {
                open_attraction_dialog(hotspot.id, mwin.clone());
            });

            // Handler for Copy
            let hotspot = hotspot_.clone();
            let mwin = map_win.clone();
            copy_item.connect_activate(move |_| {
                debug!("copy attraction {:?}!", hotspot.id);
                let atlas = &mwin.atlas.borrow();
                if let Some(attr) = atlas.attractions.get(&hotspot.id) {
                    clipboard::put(clipboard::Item::Attraction{attraction: attr.clone()})
                }
            });

            // Handler for Cut
            let hotspot = hotspot_.clone();
            let mwin = map_win.clone();
            cut_item.connect_activate(move |_| {
                debug!("cut attraction {:?}!", hotspot.id);

                // Copy to clipboard
                {
                    let atlas = &mwin.atlas.borrow();
                    if let Some(attr) = atlas.attractions.get(&hotspot.id) {
                        clipboard::put(clipboard::Item::Attraction{attraction: attr.clone()})
                    }
                }

                // Delete from atlas
                {
                    let command = Command::make_delete_attraction(hotspot.id, &mwin.atlas.borrow());
                    if let Some(cmd) = command {
                        mwin.atlas.borrow_mut().execute(&cmd);
                    }
                }

                mwin.update_map();
            });

            // Handler for Paste
            paste_item.set_sensitive(false);

            // Handler for Delete
            let hotspot = hotspot_.clone();
            let mwin = map_win.clone();
            delete_item.connect_activate(move |_| {
                let command = Command::make_delete_attraction(hotspot.id, &mwin.atlas.borrow());
                if let Some(cmd) = command {
                    mwin.atlas.borrow_mut().execute(&cmd);
                    mwin.update_map();
                }
            });
        } else {
            cut_item.set_sensitive(false);
            delete_item.set_sensitive(false);
            properties_item.set_sensitive(false);
            warn!("Unrecognize id for context menu: {}", hotspot_.id);
        }
    } else {
        cut_item.set_sensitive(false);
        delete_item.set_sensitive(false);
        properties_item.set_sensitive(false);

        // Handler for Copy (coordinates)
        let cc = cc0.clone();
        copy_item.connect_activate(move |_| {
            debug!("copy coordinates action!");
            clipboard::put(clipboard::Item::Coordinates{location: cc.clone().wpos_to_loc(wpos)})
        });

        // Handlers for Paste
        let mwin = map_win.clone();
        let cc = cc0.clone();
        paste_item.connect_activate(move |_| {
            debug!("paste action!");

            let loc = cc.clone().wpos_to_loc(wpos);
            if let Some(item) = clipboard::get() {
                match item {
                    clipboard::Item::Coordinates{location} => {
                    },
                    clipboard::Item::Attraction{attraction} => {
                        // Paste new attraction
                        match mwin.atlas.borrow_mut().next_id() {
                            Ok(id) => {
                                let mut attr = attraction.duplicate(id);
                                attr.location = loc;
                                let cmd = Command::CreateAttraction{new: attr};
                                mwin.atlas.borrow_mut().execute(&cmd);
                                mwin.update_map();
                            },
                            Err(msg) => {
                                error!("Failed to paste attraction: {}", msg);
                            }
                        }
                    }
                }
            }

            mwin.update_map();
        });
        paste_item.set_sensitive(clipboard::is_some());
    }
    
    context_menu.show_all();
    context_menu.popup_easy(event.get_button(), event.get_time());
}

