// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::{BTreeSet, HashMap, HashSet};

extern crate pango;
//use self::pango::{LayoutExt}; // TODO: REMOVE if builds without this line

use core::*;
use tiles::{TileRequest, TileCache, TileSource};
use geocoord::{Vector, VectorBox, GeoBox};
use gui::mapcanvas::{CoordinateContext};
use gui::textanchor::{TextAnchor};
use gui::sprite::{Sprite};

/// Square of hotspot select range.
const HOTSPOT_SNAP_DISTANCE2: f64 = (12.0 * 12.0);

/// Collection of element drawers that draw onto `MapCanvas`. The drawers are quaranteed to live
/// as long as the elements are visible in the canvas.
pub struct ElementDrawers {
    /// The area covering the drawers, typically larger than the visible map canvas
    pub current_area: Option<GeoBox>,
    
    /// The drawers taking care of element drawing
    pub attraction_drawers: HashMap<UniqueId, AttractionDrawer>,
    
    /// The hotspot mouse pointer is over currently
    pub hover_hotspot: Option<Hotspot>,
    
    /// The selected hotspot
    pub selected_hotspot: Option<Hotspot>,
    
    /// The current move command being constructed
    pub current_command: Option<Command>,
    
    /// Used during gather
    hidden_ids: HashSet<UniqueId>,
}

impl ElementDrawers {
    /// Default constructor
    pub fn new() -> ElementDrawers {
        ElementDrawers {
            current_area: None,
            attraction_drawers: HashMap::new(),
            hover_hotspot: None,
            selected_hotspot: None,
            hidden_ids: HashSet::new(),
            current_command: None,
        }
    }

    /// Checks if the given area is either partially or completely outside the current area.
    pub fn gather_needed(&mut self, area: &GeoBox) -> bool {
        if let Some(ca) = self.current_area {
            // If any of the corners is outside of the visible area, we need a refresh
            !ca.contains(&area.northwest()) || !ca.contains(&area.southeast())
        } else {
            // If no area is defined, we definitely need to gather
            true
        }
    }
    
    /// Look for visible elements and create drawers for them. Parameter `area` is
    /// visible coordinate area of the canvas, with possible margins.
    pub fn gather_visible(&mut self, area: &GeoBox, zoom_level: u8, atlas: &Atlas, cc: &mut CoordinateContext) {
        let narrow = area.is_narrow();
        
        self.current_area = Some(area.clone());
    
        // Collect attraction drawers
        self.hidden_ids.clear();
        for id in self.attraction_drawers.keys() {
            self.hidden_ids.insert(*id);
        }
        for (id, attr) in &atlas.attractions {
            if attr.is_visible_on_zoom_level(zoom_level) {
                //debug!("area.contains(attr): area={}, attr.loc={}, res={}", area, attr.location, area.contains_with_narrow(&attr.location, narrow));
                if area.contains_with_narrow(&attr.location, narrow) {
                    if !self.attraction_drawers.contains_key(id) {
                        let drawer = AttractionDrawer::new(cc.gb_to_vb(&attr.bounding_box()));
                        self.attraction_drawers.insert(*id, drawer);
                    }
                    self.hidden_ids.remove(id);
                }
            }
        }
        for id in &self.hidden_ids {
            self.attraction_drawers.remove(id);
        }
    }

    /// Gather hotspots for elements in the visible area.
    pub fn gather_hotspots(&mut self, area: &GeoBox, atlas: &Atlas, cc: &mut CoordinateContext) {
        // Attractions
        let mut hotspots: HashSet<Hotspot> = HashSet::new();
        for (id, drawer) in &mut self.attraction_drawers {
            if let Some(ref attr) = atlas.attractions.get(&id) {
                hotspots.clear();
                attr.gather_hotspots(area, &mut hotspots);
                if hotspots.len() == 1 {
                    for hotspot in &hotspots {
                        let mut hs = hotspot.clone();
                        hs.pos = Some(cc.loc_to_gpos(hs.location));
                        drawer.hotspot = Some(hs);
                    }
                } else {
                    debug!("Number of returned hotspots for attraction {:?} is unexpected {:?}!", 
                           id, hotspots.len());
                }
            } else {
                warn!("Attraction {:?} not found for drawer when gathering hotspots!", id);
            }
        }
    }

    /// Called when canvas is clicked or mouse moved over it. Returns true if a hotspot 
    /// status was changed.
    pub fn touch_or_hover(&mut self, mouse_gpos: &Vector, touch: bool) -> bool {
        let mut best = BestHotspot::new();
    
        // Attractions
        for (id, drawer) in &mut self.attraction_drawers {
            if let Some(ref hotspot) = drawer.hotspot {
                best.check(hotspot, mouse_gpos);
            }
        }
        
        // Save the best one (or none)
        if touch {
            let changed = { self.selected_hotspot != best.hotspot };
            debug!("touch: selected_hotspot={:?} best.hotspot={:?}", self.selected_hotspot, best.hotspot);
            self.selected_hotspot = best.hotspot;
            changed
        } else {
            let changed = { self.hover_hotspot != best.hotspot };
            self.hover_hotspot = best.hotspot;        
            changed
        }
    }
    
    /// Draw all visible elements.
    pub fn draw_all(&mut self, c: &cairo::Context, atlas: &Atlas, cc: &mut CoordinateContext, zoom_level: u8, dark: bool, round_coordinates: bool) {
        // Attractions
        let (palette, bgcolor) = {
            if dark {
                (&atlas.colors_for_dark, Color::new(0.0, 0.0, 0.0, 0.6))
            } else {
                (&atlas.colors_for_light, Color::new(1.0, 1.0, 1.0, 0.6))
            }
        };
        for (id, drawer) in &mut self.attraction_drawers {
            if let Some(ref attr) = atlas.attractions.get(&id) {
                drawer.draw(c, attr, &atlas, cc, &self.selected_hotspot, &self.hover_hotspot, 
                            zoom_level, &palette, &bgcolor, round_coordinates);
            } else {
                warn!("Attraction {:?} not found for drawer when drawing all!", id);
            }
        }
    }
    
    /// Returns true if the given hotspot is ready for drag.
    pub fn hotspot_ready_for_drag(&mut self) -> bool {
        if let Some(ref selected) = self.selected_hotspot {
            if let Some(ref hover) = self.hover_hotspot {
                if selected == hover {
                    return true;
                }
            }
        }
        false
    }
    
    /// Create move command for an element.
    pub fn start_drag(&mut self) {
        if let Some(ref selected) = self.selected_hotspot {
            // TODO: may not be an attraction
            self.current_command = Some(
                Command::MoveAttraction{
                    id: selected.id,
                    old_location: selected.location,
                    new_location: selected.location,
            });
        } else {
            self.current_command = None;
        }
    }
    
    /// Drag the currently selected hotspot and update the related Command.
    pub fn drag(&mut self, mouse_gpos: &Vector, atlas: &mut Atlas, cc: &mut CoordinateContext) {
        let mut new_command: Option<Command> = None;
        if let Some(ref mut selected) = self.selected_hotspot {
            if let Some(ref command) = self.current_command {
                match command {
                    Command::MoveAttraction{id, old_location, new_location} => {
                        if let Some(ref mut attr) = atlas.attractions.get_mut(id) {
                            attr.location = cc.gpos_to_loc(*mouse_gpos);
                            new_command = Some(
                                Command::MoveAttraction {
                                    id: *id,
                                    old_location: *old_location,
                                    new_location: attr.location,
                                });
                            selected.location = attr.location;
                            selected.pos = Some(*mouse_gpos);
                        }
                    },
                    _ => { }
                }
            } else {
                warn!("No command for drag!");
            }
        } else {
            warn!("No hotspot to drag!");
        }
        self.current_command = new_command;
    }
    
    /// End element move and execute command.
    pub fn end_drag(&mut self, atlas: &mut Atlas) {
        if let Some(ref command) = self.current_command {
            // Make the move official
            atlas.execute(command);
        } else {
            warn!("No command found at end of drag");
        }

        // Cleanup
        self.drop_all();
        self.current_command = None;
    }
    
    // Drop off drawers and related resources.
    pub fn drop_all(&mut self) {
        self.current_area = None;
        self.attraction_drawers.clear();
        self.hover_hotspot = None;
    }
}

// ---- AttractionDrawer ---------------------------------------------------------------------------

/// Responsible for drawing attraction onto canvas.
pub struct AttractionDrawer {
    pub bounding_box: VectorBox,
    pub hotspot: Option<Hotspot>,
    pub title_sprite: Option<Sprite>,
}

impl AttractionDrawer {
    pub fn new(bounding_box: VectorBox) -> AttractionDrawer {
        AttractionDrawer {
            bounding_box: bounding_box,
            hotspot: None,
            title_sprite: None,
        }
    } 

    // Draw this attraction
    pub fn draw(&mut self, c: &cairo::Context, attr: &Attraction, atlas: &Atlas, 
                cc: &mut CoordinateContext, selected_hotspot: &Option<Hotspot>,
                hover_hotspot: &Option<Hotspot>, zoom_level: u8, 
                palette: &HashMap<ColorName, Color>, bgcolor: &Color,
                round_coordinates: bool) 
    {
        let fallback_color = Color::new(0.5, 0.5, 0.5, 1.0);
        let vround = |v: Vector| { if round_coordinates { v.round() } else { v } };

        let selected: bool = { 
            if let Some(ref self_hotspot) = self.hotspot {
                if let Some(selected_hotspot) = selected_hotspot {
                    *self_hotspot == *selected_hotspot
                } else {
                    false
                }
            } else {
                false
            }
        };

        let hover: bool = { 
            if let Some(ref self_hotspot) = self.hotspot {
                if let Some(hover_hotspot) = hover_hotspot {
                    *self_hotspot == *hover_hotspot
                } else {
                    false
                }
            } else {
                false
            }
        };

        // Symbol
        // TODO
        let wpos = cc.loc_to_wpos(attr.location);
        let dot_radius = 8.0;
        if hover && selected {
            c.set_source_rgb(0.0, 1.0, 1.0);
        } else if hover {
            c.set_source_rgb(0.0, 1.0, 0.0);
        } else if selected {
            c.set_source_rgb(0.0, 0.0, 1.0);
        } else {
            c.set_source_rgb(1.0, 0.0, 0.0);
        }
        let p = vround(wpos);
        c.arc(p.x, p.y, dot_radius, 0.0, 2.0 * std::f64::consts::PI);
        c.fill();
        
        // Title
        let margin = settings_read().map_title_margin_px + 
                     settings_read().map_title_padding_px + 
                     dot_radius;
        if self.title_sprite.is_none() {
            self.title_sprite = make_title_sprite(&attr.name, 
                                                  TextAnchor::North{ margin: margin }, 
                                                  &palette.get(&attr.get_color_name(&atlas))
                                                          .unwrap_or(&fallback_color), 
                                                  bgcolor).ok();
        }
        if let Some(ref sprite) = self.title_sprite {
            let p = vround(wpos + sprite.offset);
            sprite.paint_on(c, p.x, p.y);
        }
    }
}

// ---- BestHotspot --------------------------------------------------------------------------------

pub struct BestHotspot {
    pub hotspot: Option<Hotspot>,
    pub dist2: f64,
}

impl BestHotspot {
    pub fn new() -> BestHotspot {
        BestHotspot {
            hotspot: None,
            dist2: 0.0,
        }
    }

    /// Test whether the given mouse position is within snap range. If that's true and the range
    /// is a new minimum, save the given hotspot as the best.    
    pub fn check(&mut self, hotspot: &Hotspot, mouse_gpos: &Vector) {
        if let Some(gpos) = hotspot.pos {
            let dist2 = (gpos - *mouse_gpos).length2();
            if dist2 <= HOTSPOT_SNAP_DISTANCE2 {
                if dist2 < self.dist2 || self.hotspot.is_none() {
                    self.hotspot = Some(hotspot.clone());
                    self.dist2 = dist2;
                }
            }            
        }
    }
}

// -------------------------------------------------------------------------------------------------

/// Draw the tiles onto a sprite.
///
/// # Arguments
///
/// * `sprite` - A `Sprite` to be used as draw context.
///
/// * `center_pos` - Global pixel pos for view center
///
/// * `focus_pos` - Global pixel pos for current focus point
///
/// * `cc` - A helper for coordinate transformations.
///
/// * `tile_source` - `TileSource` object.
///
/// * `tile_cache` - `TileCache` object.
///
///
pub fn draw_tiles(sprite: &mut Sprite, center_pos: Vector, focus_pos: Vector, 
                  cc: &mut CoordinateContext, tile_source: &TileSource, tcache: &mut TileCache) 
{
    let sw = sprite.width() as f64;
    let sh = sprite.height() as f64;
    let tw = tile_source.tile_width as f64;
    let th = tile_source.tile_height as f64;
    let gnw_pos = cc.global_northwest_pos();
    let snwx = center_pos.x - sw / 2.0 - gnw_pos.x;
    let snwy = center_pos.y - sh / 2.0 - gnw_pos.y;
    let grid_w = (sw / tw).ceil() as i32;
    let grid_h = (sh / th).ceil() as i32;
    let grid_x = (snwx / tw) as i32;
    let grid_y = (snwy / th) as i32;
    let grid_x_mod = snwx % tw;
    let grid_y_mod = snwy % th;
    let offset_pos = Vector::new(
        sw / 2.0 + grid_x_mod, 
        sh / 2.0 + grid_y_mod);
    let zoom_level = sprite.zoom_level.unwrap();
    let mult = 1; // TODO

    // Ensure that offset and zoom level are correct in the sprite
    sprite.offset = offset_pos;

    // Create an ordered list of tile requests
    let mut treqs: BTreeSet<TileRequest> = BTreeSet::new();
    let gen = chrono::prelude::Utc::now().timestamp() as u64;
    for ly in 0..grid_h {
        for lx in 0..grid_w {
            // Priority depends on difference between tile center and focus point
            let tile_xy = Vector::new(lx as f64 * tw + tw/2.0, 
                                     ly as f64 * th + th/2.0);
            let focus_xy = focus_pos - center_pos + offset_pos;
            let pri = -(tile_xy - focus_xy).length2();
            
            // Add to the ordered set
            let treq = TileRequest::new(gen, pri as i64,
                grid_x + lx as i32, 
                grid_y + ly as i32, 
                zoom_level, mult, tile_source.clone());
            treqs.insert(treq);    
        }
    }

    // Clear surface
    let tc = sprite.to_context();
    tc.set_source_rgb(0.8, 0.8, 0.8);
    tc.paint();
    
    // Request tiles
    for treq in treqs.iter().rev() {
        // Handle the response
        if let Some(tile) = tcache.get_tile(&treq) {
            //debug!("draw_tiles: tile={:?}", tile);
            // Draw tile
            if let Some(ref tile_surface) = tile.get_surface() {
                // Draw tile onto sprite                                        
                let lx = treq.x - grid_x;
                let ly = treq.y - grid_y;
                let vx = lx as f64 * tw;
                let vy = ly as f64 * th;
                tc.set_source_surface(tile_surface, vx, vy);
                tc.paint();
            } else {
                warn!("No surface!");
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------

/// Make sprite with the given title. Parameter `anchor` affects to the Sprite offset vector.
/// If parameter `light` is true it's expected that background of the context is light.
fn make_title_sprite(title: &String, anchor: TextAnchor, color: &Color, bgcolor: &Color) -> Result<Sprite, String> {
    let scale = 1.0;
    let font_size = settings_read().map_font_size_pt;
    let padding = settings_read().map_title_padding_px;

    let apply_font = |c| { 
        if let Some(layout) = pangocairo::functions::create_layout(&c) {
            // Define font and text layout
            let mut font_desc = pango::FontDescription::new();
            font_desc.set_weight(pango::Weight::Normal);
            font_desc.set_family("Sans");
            font_desc.set_style(pango::Style::Normal);
            font_desc.set_size((scale * (pango::SCALE as f64) * font_size).round() as i32);
            layout.set_font_description(Some(&font_desc));
            layout.set_width(1000);
            layout.set_height(1000 * pango::SCALE);
            layout.set_text(&title);
            let (ink_rect, logical_rect) = layout.get_extents();
            let tw = (logical_rect.width / pango::SCALE) as f64;
            let th = (logical_rect.height / pango::SCALE) as f64;
            (tw.ceil() as i32, th.ceil() as i32, Some(layout))
        } else {
            warn!("No Pango layout!");
            (400, 40, None)
        }
    };

    // Measure text
    let (tw, th, _) = match Sprite::with_size_and_transparency(16, 16) {
        Ok(mut sprite) => {
            apply_font(sprite.to_context())
        },
        Err(e) => {
            warn!("Failed to create sprite for attraction: {}", e);
            (400, 40, None)
        }
    };
    let sw = tw * (2.0 * padding) as i32;
    let sh = th * (2.0 * padding) as i32;
    
    match Sprite::with_size_and_transparency(sw, sh) {
        Ok(mut sprite) => {
            let c = sprite.to_context();
            if let Some(layout) = pangocairo::functions::create_layout(&c) {
                // Define font and text layout
                let (tw, th, layout_o) = apply_font(sprite.to_context());
                
                if let Some(layout) = layout_o {
                    let th = th as f64;
                    let tw = tw as f64;
                    let sw = sw as f64;
                    let sh = sh as f64;
                    let center = Vector::new(sw / 2.0, sh / 2.0);
                    sprite.offset = (anchor.make_offset(tw, th) - center).round();

                    // Draw semi-transparent background box with rounded corners
                    if settings_read().map_title_background_boxes {
				        let radius = padding;
				        let deg = std::f64::consts::PI / 180.0;
				        let bx = center.x - tw / 2.0 - radius;
				        let by = center.y - th / 2.0 - radius;
				        let bw = tw + 2.0 * radius;
				        let bh = th + 2.0 * radius;

				        c.new_sub_path();
				        c.arc(bx + bw - radius, by + radius, radius, -90.0 * deg, 0.0 * deg);
				        c.arc(bx + bw - radius, by + bh - radius, radius, 0.0 * deg, 90.0 * deg);
				        c.arc(bx + radius, by + bh - radius, radius, 90.0 * deg, 180.0 * deg);
				        c.arc(bx + radius, by + radius, radius, 180.0 * deg, 270.0 * deg);
				        c.close_path ();

                        c.set_source_rgba (bgcolor.red, bgcolor.green, bgcolor.blue, bgcolor.alpha);
				        c.fill ();
                    }                    

                    // Draw text in the center of the sprite
                    {
                        c.set_source_rgb(color.red, color.green, color.blue);
                        c.move_to((sw - tw) / 2.0, (sh - th) / 2.0);
                        pangocairo::functions::update_layout(&c, &layout);
                        pangocairo::functions::show_layout(&c, &layout);
                    }

                    /* 
                    // Yellow corners for debugging
                    c.set_source_rgb(1.0, 1.0, 0.0);
                    c.arc(0.0, 0.0, 8.0, 0.0, 2.0 * std::f64::consts::PI); c.fill();
                    c.arc(sw, 0.0, 8.0, 0.0, 2.0 * std::f64::consts::PI); c.fill();
                    c.arc(sw, sh, 8.0, 0.0, 2.0 * std::f64::consts::PI); c.fill();
                    c.arc(0.0, sh, 8.0, 0.0, 2.0 * std::f64::consts::PI); c.fill();
                    */                    
                    
                    // Save sprite
                    Ok(sprite)
                } else {
                    Err("Missing pango layout!".into())
                }
            } else {
                Err("Failed to create pango layout".into())
            }
        },
        Err(e) => {
            error!("Failed to create sprite for attraction: {}", e);
            Err(format!("Failed to create sprite for attraction: {}", e))
        }
    }
}

