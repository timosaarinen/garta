// Garta - A geography application for GTK3
// Copyright (C) 2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::{HashSet, HashMap};
use std::rc::{Rc};
use std::cell::{RefCell};
use std::convert::{TryInto};

extern crate chrono;
use self::chrono::{NaiveDate};
use self::chrono::prelude::*;

extern crate reqwest;
//extern crate futures;

extern crate gtk;
use self::gtk::prelude::*;

use core::{Attraction, MapElement, ElementField, ElementFieldValue, Command, UniqueId, ColorRule, COLOR_NAMES, settings_read, Slug};
use geocoord::{Location};

use gui::mapwindow::{MapWindow};
//use gui::sprite::{fetch_sprite};

/// Show Attraction dialog.
pub fn open_attraction_dialog(attr_id: UniqueId, map_win: Rc<MapWindow>) {
    // Make two copies of the Attraction
    let orig_attr = {
        let atlas = map_win.atlas.borrow();
        if let Some(attr_) = atlas.attractions.get(&attr_id) {
            Rc::new(RefCell::new(attr_.clone()))
        } else {
            Rc::new(RefCell::new(Attraction::new(
                    0, "".into(), Location::new(0.0, 0.0))))
        }
    };
    if orig_attr.borrow().id() == 0 {
        warn!("No attraction found for id {}", attr_id);
        return;
    }

    // Set widget values
    let builder = gtk::Builder::new_from_file(settings_read().ui_directory_for("attraction.ui"));
    if let Some(ref win) = builder.get_object("attraction_window") as Option<gtk::Window> {
        // Type combo
        {
            let atlas = map_win.atlas.borrow();
            let combo: gtk::ComboBox = builder.get_object("type")
                                              .expect("No attraction type combo box!");
            let col_types: [glib::Type; 2] = [
                glib::Type::String, 
                glib::Type::String, 
            ];
            let store = gtk::ListStore::new(&col_types);
            let i = 0u32;
            let mut active_id = String::new();
            for (kind_slug, kind) in &atlas.attraction_kinds {
                let col_indices: [u32; 2] = [0, 1];
                let values: [&dyn ToValue; 2] = [
                    &kind.name,
                    kind_slug,
                ];
                store.set(&store.append(), &col_indices, &values);
                
                if *kind_slug == orig_attr.borrow().kind {
                    active_id = kind_slug.clone();
                }
            }
            
            combo.set_model(Some(&store));
            combo.set_id_column(1);
            
            let renderer = gtk::CellRendererText::new();
            combo.pack_start(&renderer, true);
            combo.add_attribute(&renderer, "text", 0);

            let r = combo.set_active_id(Some(active_id.as_str()));
        }
    
        // Name entry
        {
            let name_entry: gtk::Entry = builder.get_object("name")
                                                .expect("No attraction name widget!");
            name_entry.set_text(&orig_attr.borrow().name);
        }

        // Color combo
        {
            let atlas = map_win.atlas.borrow();
            let combo: gtk::ComboBox = builder.get_object("color")
                                              .expect("No attraction color combo box!");
            let col_types: [glib::Type; 2] = [
                glib::Type::String, 
                glib::Type::String, 
            ];
            let store = gtk::ListStore::new(&col_types);
            let i = 0u32;
            let mut active_id = String::new();
            let col_indices: [u32; 2] = [0, 1];
            {
                // Add special item for kind-derived color
                let item_name = {
                    if let Some(kind) = atlas.attraction_kinds.get(&orig_attr.borrow().kind) {
                        match &kind.color_rule {
                            ColorRule::UserDefineable{} => {
                                String::from("default") // TODO: localize
                            },
                            ColorRule::Fixed{color} => {
                                String::from("type-based") // TODO: localize
                            },
                            _ => {
                                String::from("rule-based") // TODO: localize
                            },
                        }
                    } else {
                        String::from("default") // TODO: localize
                    }
                }; 
                let item_slug = String::from("");
                let values: [&dyn ToValue; 2] = [
                    &item_name,
                    &item_slug,
                ];
                store.set(&store.append(), &col_indices, &values);
                active_id = item_slug.clone();
            }
            let attraction_color_name = orig_attr.borrow().color.clone();
            for color_name in &COLOR_NAMES {
                let localized_name = color_name.to_string(); // TODO: localization
                let color_slug = color_name.to_string();
                let values: [&dyn ToValue; 2] = [
                    &localized_name,
                    &color_slug,
                ];
                store.set(&store.append(), &col_indices, &values);
                
                if let Some(ref acn) = attraction_color_name { 
                    if color_name == acn {
                        active_id = color_slug.clone();
                    }
                } 
            }
            
            combo.set_model(Some(&store));
            combo.set_id_column(1);
            
            let renderer = gtk::CellRendererText::new();
            combo.pack_start(&renderer, true);
            combo.add_attribute(&renderer, "text", 0);

            let r = combo.set_active_id(Some(active_id.as_str()));
        }
        
        // Min zoom level
        {
            let spin: gtk::SpinButton = builder.get_object("visibility_spinbutton_from")
                                               .expect("No attraction min visibility widget!");
            spin.set_range(1.0, 18.0); // TODO: no hard-coded values
            spin.set_snap_to_ticks(true);
            spin.set_increments(1.0, 4.0);
            if let Some(min_zoom_level) = orig_attr.borrow().min_zoom_level {
                spin.set_value(min_zoom_level as f64);
            } else {
                spin.set_value(1.0); // TODO: no hard-coded value
            }
        }

        // Min zoom level
        {
            let spin: gtk::SpinButton = builder.get_object("visibility_spinbutton_to")
                                               .expect("No attraction max visibility widget!");
            spin.set_range(1.0, 18.0); // TODO: no hard-coded values
            spin.set_snap_to_ticks(true);
            spin.set_increments(1.0, 4.0);
            if let Some(max_zoom_level) = orig_attr.borrow().max_zoom_level {
                spin.set_value(max_zoom_level as f64);
            } else {
                spin.set_value(18.0); // TODO: no hard-coded value
            }
        }

        // Description
        {
            let tview: gtk::TextView = builder.get_object("description")
                                              .expect("No attraction description widget!");
            if let Some(buffer) = tview.get_buffer() {
                buffer.set_text(&orig_attr.borrow().description);
            } else {
                warn!("No text view buffer for description!");
            }
            
            // TODO: implement undo/redo! ~ https://stackoverflow.com/questions/76096/undo-with-gtk-textview
        }
        
        // Specific
        let field_values = Rc::new(RefCell::new(orig_attr.borrow().fields.clone()));
        debug!("field_values: {:?}", field_values);
        {
            let mut exclamation_labels = Vec::<gtk::Label>::new();
            let mut show_fields_tab = false;
            let grid: gtk::Grid = builder.get_object("fields_inner_grid")
                                         .expect("No attraction fields inner grid!");
            grid.set_row_spacing(8);                                         
            grid.set_column_spacing(8);                                         
            let atlas = map_win.atlas.borrow();
            if let Some(kind) = atlas.attraction_kinds.get(&orig_attr.borrow().kind) {
                // Hide fields tab if there aren't any widgets defined
                if kind.fields.len() > 0 {
                    show_fields_tab = true;
                }
                
                // Add fields as widgets
                let mut i = 0;
                for field in &kind.fields {
                    match field {
                        ElementField::TextField{slug, title, max_len, regex} => {
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);
                            
                            // Entry
                            let widget = gtk::Entry::new();
                            if let Some(mlen) = max_len {
                                widget.set_max_length(*mlen as i32);
                            }
                            grid.attach(&widget, 1, i, 2, 1);
                            if let Some(field_value) = field_values.borrow().get(slug) {
                                match field_value {
                                    ElementFieldValue::Text{value} => {
                                        widget.set_text(value);
                                    },
                                    _ => {
                                        warn!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }
                            
                            // Exclamation label
                            let exclamation_label = make_exclamation_label();
                            exclamation_labels.push(exclamation_label.clone());
                            grid.attach(&exclamation_label, 3, i, 1, 1);
                            i += 1;

                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                let text = entry.get_text().unwrap_or("".into());
                                let mut valid = true;
                                if text != "" {
                                    let val = ElementFieldValue::Text{ value: text.into() };
                                    if field.valid(&val) {
                                        fvs.borrow_mut().insert(slug.clone(), val);
                                    } else {
                                        debug!("invalid: {:?}", val);
                                        valid = false;
                                    }
                                }

                                if valid {
                                    exclamation_label.hide();
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                    exclamation_label.show();
                                }
                            });
                        },
                        ElementField::IntegerField{slug, title, min_value, max_value, suffix} => {
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);
                            
                            // Entry
                            let widget = gtk::Entry::new();
                            widget.set_input_purpose(gtk::InputPurpose::Number);
                            if let Some(suffix) = suffix {
                                grid.attach(&widget, 1, i, 1, 1);
                                {
                                    let label = gtk::Label::new(Some(suffix.as_str()));
                                    label.set_xalign(0.0);
                                    grid.attach(&label, 2, i, 1, 1);
                                }
                            } else {
                                grid.attach(&widget, 1, i, 2, 1);
                            }
                            if let Some(field_value) = field_values.borrow().get(slug) {
                                match field_value {
                                    ElementFieldValue::Integer{value} => {
                                        widget.set_text(&value.to_string());
                                    },
                                    _ => {
                                        widget.set_text(
                                            format!("{}", min_value.unwrap_or(0)).as_str());
                                        debug!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }

                            // Exclamation label
                            let exclamation_label = make_exclamation_label();
                            exclamation_labels.push(exclamation_label.clone());
                            grid.attach(&exclamation_label, 3, i, 1, 1);
                            i += 1;

                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                let text = entry.get_text().unwrap_or("".into());
                                let mut valid = true;
                                if text != "" {
                                    match text.parse::<i64>() {
                                        Ok(i) => {
                                            let val = ElementFieldValue::Integer{ value: i };
                                            if field.valid(&val) {
                                                fvs.borrow_mut().insert(slug.clone(), val);
                                            } else {
                                                debug!("invalid: {:?}", val);
                                                valid = false;
                                            }
                                        },
                                        Err(e) => {
                                            debug!("{}", e);
                                            valid = false;
                                        }
                                    }
                                }

                                if valid {
                                    exclamation_label.hide();
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                    exclamation_label.show();
                                }
                            });
                        },
                        ElementField::FloatField{slug, title, decimals, min_value, max_value, suffix} => {
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);
                            
                            // Entry
                            let widget = gtk::Entry::new();
                            widget.set_input_purpose(gtk::InputPurpose::Number);
                            if let Some(suffix) = suffix {
                                grid.attach(&widget, 1, i, 1, 1);
                                {
                                    let label = gtk::Label::new(Some(suffix.as_str()));
                                    label.set_xalign(0.0);
                                    grid.attach(&label, 2, i, 1, 1);
                                }
                            } else {
                                grid.attach(&widget, 1, i, 2, 1);
                            }
                            if let Some(field_value) = field_values.borrow().get(slug) {
                                match field_value {
                                    ElementFieldValue::Decimal{value} => {
                                        // TODO: decimals
                                        widget.set_text(format!("{}", value).as_str());
                                    },
                                    _ => {
                                        widget.set_text(
                                            format!("{}", min_value.unwrap_or(0.0)).as_str());
                                        warn!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }

                            // Exclamation label
                            let exclamation_label = make_exclamation_label();
                            exclamation_labels.push(exclamation_label.clone());
                            grid.attach(&exclamation_label, 3, i, 1, 1);
                            i += 1;

                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                let text = entry.get_text().unwrap_or("".into());
                                let mut valid = true;
                                if text != "" {
                                    match text.parse::<f64>() {
                                        Ok(f) => {
                                            let val = ElementFieldValue::Decimal{ value: f };
                                            if field.valid(&val) {
                                                fvs.borrow_mut().insert(slug.clone(), val);
                                            } else {
                                                debug!("invalid: {:?}", val);
                                                valid = false;
                                            }
                                        },
                                        Err(e) => {
                                            debug!("{}", e);
                                            valid = false;
                                        }
                                    }
                                }

                                if valid {
                                    exclamation_label.hide();
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                    exclamation_label.show();
                                }
                            });
                        },
                        ElementField::ComboField{slug, title, values} => {
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);
                            
                            // Widget
                            let widget = make_simple_combobox(values, values, &values[0]);
                            grid.attach(&widget, 1, i, 2, 1);
                            if let Some(field_value) = field_values.borrow().get(slug) {
                                match field_value {
                                    ElementFieldValue::Text{value} => {
                                        if value != "" {
                                            widget.set_active_id(Some(value.as_ref()));
                                        } else {
                                            widget.set_active_id(None);
                                        }
                                    },
                                    _ => {
                                        warn!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }
                            i += 1;
                            
                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                fvs.borrow_mut().insert(slug.clone(), ElementFieldValue::Text{
                                    value: entry.get_active_id().unwrap_or("".into()).into()
                                });
                            });
                        },
                        ElementField::CheckboxField{slug, title, values} => {
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);

                            // Widgets
                            for value in values {
                                let row = gtk::ListBoxRow::new();
                                let button = gtk::CheckButton::new_with_label(&value);
                                
                                // Selected value
                                if let Some(field_value) = field_values.borrow().get(slug) {
                                    match field_value {
                                        ElementFieldValue::MultiText{mvalue} => {
                                            button.set_active(mvalue.contains(value));
                                        },
                                        _ => {
                                            warn!("Unexpected ElementFieldValue type for {}", slug);
                                        }
                                    }
                                }
                                
                                // Signal handler
                                let fvs = field_values.clone();
                                let slug = slug.clone();
                                let field = field.clone();
                                let value = value.clone();
                                button.connect_toggled(move |b| {
                                    if !fvs.borrow().contains_key(&slug) {
                                        fvs.borrow_mut().insert(slug.clone(), 
                                            ElementFieldValue::MultiText{
                                                mvalue: HashSet::new(),
                                            }
                                        );
                                    }
                                    match fvs.borrow_mut().get_mut(&slug).unwrap() {
                                        ElementFieldValue::MultiText{ mvalue } => {
                                            if b.get_active() {
                                                mvalue.insert(value.clone());
                                            } else {
                                                mvalue.remove(&value);
                                            }
                                        },
                                        _ => {
                                            warn!("Unexpected ElementFieldValue type for {}", slug);
                                        }
                                    }
                                });                

                                // Add button to row and the row to grid
                                row.add(&button);
                                grid.attach(&row, 1, i, 2, 1);
                                i += 1;
                            }
                        },
                        ElementField::DateField{slug, title} => {
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);

                            // Entry
                            let widget = gtk::Entry::new();
                            widget.set_max_length(10);
                            grid.attach(&widget, 1, i, 2, 1);
                            if let Some(field_value) = field_values.borrow().get(slug) {
                                match field_value {
                                    ElementFieldValue::Date{value} => {
                                        widget.set_text(format!("{}", value).as_str());
                                    },
                                    _ => {
                                        warn!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }
                            
                            // Exclamation label
                            let exclamation_label = make_exclamation_label();
                            exclamation_label.set_tooltip_text(Some("YYYY-MM-DD or today"));
                            exclamation_labels.push(exclamation_label.clone());
                            grid.attach(&exclamation_label, 3, i, 1, 1);
                            i += 1;

                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                let text = entry.get_text().unwrap_or("".into());
                                let mut valid = true;
                                if text == "today" || text == "now" { // TODO: localize
                                    fvs.borrow_mut().insert(slug.clone(), ElementFieldValue::Date{ 
                                        value: Local::now().date().naive_utc()
                                    });
                                } else if text != "" {
                                    match text.parse::<NaiveDate>() {
                                        Ok(nd) => {
                                            let val = ElementFieldValue::Date{ value: nd };
                                            if field.valid(&val) {
                                                fvs.borrow_mut().insert(slug.clone(), val);
                                            } else {
                                                debug!("invalid: {:?}", val);
                                                valid = false;
                                            }
                                        },
                                        Err(e) => {
                                            valid = false;
                                        }
                                    }
                                }

                                if valid {
                                    exclamation_label.hide();
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                    exclamation_label.show();
                                }
                            });
                        },
                        ElementField::MonthDayRangeField{slug, title} => {
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);

                            // Current value
                            let (month_from, day_from, month_to, day_to) = 
                                get_month_day_value(&field_values, &slug, &field);

                            // From month
                            let month_keys = vec![
                                "1".into(), "2".into(), "3".into(), "4".into(), 
                                "5".into(), "6".into(), "7".into(), "8".into(),
                                "9".into(), "10".into(), "11".into(), "12".into(),
                            ];
                            let month_names = vec![ // TODO: localize
                                "January".into(), "February".into(), "March".into(), 
                                "April".into(),   "May".into(),      "June".into(),
                                "July".into(),    "August".into(),   "September".into(),
                                "October".into(), "November".into(), "December".into(),
                            ];
                            let from_month_widget = make_simple_combobox(
                                                        &month_keys, &month_names, &month_keys[0]);
                            if let Some(value) = month_from {
                                from_month_widget.set_active_id(Some(format!("{}", value).as_str()));
                            } else {
                                from_month_widget.set_active_id(Some("1"));
                            }
                            grid.attach(&from_month_widget, 1, i, 1, 1);

                            // From day
                            let (day_keys, day_names) = generate_month_days(31, true);
                            let from_day_widget = make_simple_combobox(
                                                        &day_keys, &day_names, &day_keys[0]);
                            if let Some(value) = day_from {
                                from_day_widget.set_active_id(Some(format!("{}", value).as_str()));
                            } else {
                                from_day_widget.set_active_id(Some(""));
                            }
                            grid.attach(&from_day_widget, 2, i, 1, 1);
                            i += 1;

                            // To month
                            let to_month_widget = make_simple_combobox(
                                                        &month_keys, &month_names, 
                                                        &month_keys[&month_keys.len() - 1]);
                            if let Some(value) = month_to {
                                to_month_widget.set_active_id(Some(format!("{}", value).as_str()));
                            } else {
                                to_month_widget.set_active_id(Some("12"));
                            }
                            grid.attach(&to_month_widget, 1, i, 1, 1);

                            // To day
                            let (day_keys, day_names) = generate_month_days(31, false);
                            let to_day_widget = make_simple_combobox(
                                                        &day_keys, &day_names, 
                                                        &day_keys[day_keys.len() - 1]);
                            if let Some(value) = day_to {
                                to_day_widget.set_active_id(Some(format!("{}", value).as_str()));
                            } else {
                                to_day_widget.set_active_id(Some(""));
                            }
                            grid.attach(&to_day_widget, 2, i, 1, 1);
                            i += 1;

                            // Connect change signals (from month)
                            {
                                let fvs = field_values.clone();
                                let slug = slug.clone();
                                let field = field.clone();
                                from_month_widget.connect_changed(move |widget| {
                                    let value: u8 = widget.get_active_id()
                                                          .unwrap_or("1".into())
                                                          .parse::<u8>().unwrap_or(1);
                                    set_month_day_value(&fvs, &slug, &field, 
                                        Some(Some(value)), None, None, None);
                                });
                            }

                            // Connect change signals (from day)
                            {
                                let fvs = field_values.clone();
                                let slug = slug.clone();
                                let field = field.clone();
                                from_day_widget.connect_changed(move |widget| {
                                    match widget.get_active_id()
                                                .unwrap_or("".into())
                                                .parse::<u8>()
                                    {
                                        Ok(value) => {
                                            set_month_day_value(&fvs, &slug, &field,
                                                None, Some(Some(value)), None, None);
                                        },
                                        Err(e) => {
                                            set_month_day_value(&fvs, &slug, &field,
                                                None, Some(None), None, None);
                                        }
                                    }
                                });
                            }

                            // Connect change signals (to month)
                            {
                                let fvs = field_values.clone();
                                let slug = slug.clone();
                                let field = field.clone();
                                to_month_widget.connect_changed(move |widget| {
                                    let value: u8 = widget.get_active_id()
                                                          .unwrap_or("12".into())
                                                          .parse::<u8>().unwrap_or(12);
                                    set_month_day_value(&fvs, &slug, &field,
                                        None, None, Some(Some(value)), None);
                                });
                            }

                            // Connect change signals (to day)
                            {
                                let fvs = field_values.clone();
                                let slug = slug.clone();
                                let field = field.clone();
                                to_day_widget.connect_changed(move |widget| {
                                    match widget.get_active_id()
                                                .unwrap_or("".into())
                                                .parse::<u8>()
                                    {
                                        Ok(value) => {
                                            set_month_day_value(&fvs, &slug, &field,
                                                None, None, None, Some(Some(value)));
                                        },
                                        Err(e) => {
                                            set_month_day_value(&fvs, &slug, &field,
                                                None, None, None, Some(None));
                                        }
                                    }
                                });
                            }

                        },
                        ElementField::AltitudeField{} => {
                            let slug = "_altitude".to_string();
                            let title = "Altitude".to_string(); // TODO: localisation
                        
                            // Title
                            let label = gtk::Label::new(Some(format!("{}:", title).as_str()));
                            label.set_xalign(0.0);
                            grid.attach(&label, 0, i, 1, 1);
                            
                            // Entry
                            let widget = gtk::Entry::new();
                            widget.set_input_purpose(gtk::InputPurpose::Number);
                            grid.attach(&widget, 1, i, 1, 1);
                            {
                                let label = gtk::Label::new(Some("m".into())); // TODO: unit
                                label.set_xalign(0.0);
                                grid.attach(&label, 2, i, 1, 1);
                            }
                            if let Some(value) = orig_attr.borrow().location.elevation {
                                widget.set_text(format!("{}", value).as_str());
                                field_values.borrow_mut().insert(
                                    slug.clone(), ElementFieldValue::Decimal{ value });
                            } else {
                                widget.set_text("");
                            }
                            if let Some(field_value) = field_values.borrow().get(&slug) {
                                match field_value {
                                    ElementFieldValue::Decimal{value} => {
                                        widget.set_text(&value.to_string());
                                    },
                                    _ => {
                                        debug!("Unexpected ElementFieldValue type for {}", slug);
                                    }
                                }
                            }

                            // Exclamation label
                            let exclamation_label = make_exclamation_label();
                            exclamation_labels.push(exclamation_label.clone());
                            grid.attach(&exclamation_label, 3, i, 1, 1);
                            i += 1;

                            // Connect change signal
                            let fvs = field_values.clone();
                            let slug = slug.clone();
                            let field = field.clone();
                            widget.connect_changed(move |entry| {
                                let text = entry.get_text().unwrap_or("".into());
                                let mut valid = true;
                                if text != "" {
                                    match text.parse::<f64>() {
                                        Ok(f) => {
                                            let val = ElementFieldValue::Decimal{ value: f };
                                            if field.valid(&val) {
                                                fvs.borrow_mut().insert(slug.clone(), val);
                                            } else {
                                                debug!("invalid: {:?}", val);
                                                valid = false;
                                            }
                                        },
                                        Err(e) => {
                                            debug!("{}", e);
                                            valid = false;
                                        }
                                    }
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                }

                                if valid {
                                    exclamation_label.hide();
                                } else {
                                    fvs.borrow_mut().remove(&slug);
                                    exclamation_label.show();
                                }
                            });
                        },
                    }
                }
            } else {
                debug!("No kind defined for the attraction");
            }

            // Check if fields tab should be hidden
            let notebook: gtk::Notebook = builder.get_object("attraction_notebook")
                                                 .expect("No attraction notebook!");
            if let Some(page) = notebook.get_nth_page(Some(1)) {
                if show_fields_tab {
                    page.show_all();                   
                    for label in &exclamation_labels {
                        label.hide();
                    }
                } else {
                    page.hide();
                }
            }
        }
        
        // Layers
        let selected_layer_ids: Rc<RefCell<HashSet<UniqueId>>> = Rc::new(RefCell::new(HashSet::new()));
        {
            let layers_box: gtk::ListBox = builder.get_object("layers")
                                                  .expect("No attraction layers listbox!");
            let attr = orig_attr.borrow();

            // Add all layers to the box
            debug!("attr.layer_ids: {:?}", attr.layer_ids);
            let atlas = map_win.atlas.borrow();
            for (layer_id, layer) in &atlas.layers {
                let row = gtk::ListBoxRow::new();
                let button = gtk::CheckButton::new_with_label(&layer.name);
                
                // Selected layers
                if attr.layer_ids.contains(layer_id) {
                    button.set_active(true);
                    selected_layer_ids.borrow_mut().insert(*layer_id);
                }
                
                // Signal handler
                let b = button.clone();
                let lid = layer_id.clone();
                let slids = selected_layer_ids.clone();
                button.connect_toggled(move |_| {
                    if b.get_active() {
                        if !slids.borrow().contains(&lid) {
                            slids.borrow_mut().insert(lid);
                        }
                    } else {
                        if slids.borrow().contains(&lid) {
                            slids.borrow_mut().remove(&lid);
                        }
                    }
                });

                // Add button to row and row to layers box
                row.add(&button);
                layers_box.add(&row);
            }
            
            layers_box.show_all();
        }

        // Image
        {
            // Entry widget
            let entry: gtk::Entry = builder.get_object("image_url")
                                           .expect("No attraction image_url widget!");
            if let Some(uri) = &orig_attr.borrow().picture_uri {
                entry.set_text(uri.as_str());
            }

            // Picture widget
            let image: gtk::Image = builder.get_object("image")
                                           .expect("No attraction image widget!");
            if let Some(uri) = &orig_attr.borrow().picture_uri {
/* TODO: futures            
                let image = image.clone();
                fetch_sprite(uri, move |result| {
                    match result {
                        Ok(sprite) => {
                            sprite.apply_on(&image);
                        },
                        Err(e) => {
                            image.set_from_pixbuf(None);
                        }
                    }
                } );
*/                
            }

            // Connect change signal for Entry widget
            entry.connect_focus_out_event(move |entry, ev| {
/* TODO: futures            
                let uri = entry.get_text().unwrap_or("".into());
                if uri != "" {
                    let image = image.clone();
                    fetch_sprite(&uri.to_string(), move |result| {
                        match result {
                            Ok(sprite) => {
                                sprite.apply_on(&image);
                            },
                            Err(e) => {
                                image.set_from_pixbuf(None);
                            }
                        }
                    } );
                }
*/                
                Inhibit(false)
            });
        }

/*
        // Latitude
        {
            let entry: gtk::Entry = builder.get_object("latitude")
                                           .expect("No attraction latitude widget!");
            let lon = &orig_attr.borrow().location.lat;
            entry.set_text(&format!("{}", lon));
        }

        // Longitude
        {
            let entry: gtk::Entry = builder.get_object("longitude")
                                           .expect("No attraction longitude widget!");
            let lon = &orig_attr.borrow().location.lon;
            entry.set_text(&format!("{}", lon));
        }

        // Altitude
        {
            let entry: gtk::Entry = builder.get_object("altitude")
                                           .expect("No attraction altitude widget!");
            if let Some(elevation) = &orig_attr.borrow().location.elevation {
                entry.set_text(&format!("{} m", elevation)); // TODO: units
            } else {
                entry.set_text("".into());
            }
        }
*/        
        // Hide location tab
        {
            let notebook: gtk::Notebook = builder.get_object("attraction_notebook")
                                                 .expect("No attraction notebook!");
            if let Some(page) = notebook.get_nth_page(Some(4)) {
                page.hide();
            }
        }


        // Handle close window
        {
            let attr_win = win.clone();
            let map_win = map_win.clone();
            win.connect_delete_event(move |_, _| {
                {
                    let mut atlas = map_win.atlas.borrow_mut();
                    let mut attr = orig_attr.borrow().clone();

                    // Pick kind (type)
                    {
                        let entry: gtk::ComboBox = builder.get_object("type")
                                                          .expect("No attraction type widget!");
                        if let Some(value) = entry.get_active_id() {
                            attr.kind = value.into();
                        } else {
                            warn!("No value for attraction kind!");
                        }
                    }

                    // Pick title
                    {
                        let entry: gtk::Entry = builder.get_object("name")
                                                       .expect("No attraction name widget!");
                        attr.name = entry.get_text().unwrap_or("".into()).into();
                    }

                    // Pick color
                    {
                        let entry: gtk::ComboBox = builder.get_object("color")
                                                          .expect("No attraction color widget!");
                        if let Some(value_gs) = entry.get_active_id() {
                            let value: String = value_gs.into();
                            if value != "" {
                                match value.try_into() {
                                    Ok(v) => {
                                        attr.color = Some(v);
                                    },
                                    Err(e) => {
                                        warn!("{}", e);
                                        attr.color = None;
                                    }
                                }
                            } else {
                                attr.color = None;
                            }
                        } else {
                            warn!("No value for attraction kind!");
                        }
                    }

                    // Pick description
                    {
                        let entry: gtk::TextView = builder.get_object("description")
                                                          .expect("No attraction description widget!");
                        if let Some(buffer) = entry.get_buffer() {
                            if let Some(text) = buffer.get_text(
                                        &buffer.get_start_iter(),
                                        &buffer.get_end_iter(),
                                        true) {
                                attr.description = text.into();
                            } else {
                                attr.description = "".into();
                            }
                        } else {
                            warn!("No text view buffer for description!");
                        }
                    }

                    // Pick field values
                    {
                        if let Some(kind) = atlas.attraction_kinds.get(&attr.kind) {
                            attr.location.elevation = None;
                            for field in &kind.fields {
                                match field {
                                    ElementField::AltitudeField{} => {
                                        let slug = "_altitude".to_string();
                                        let mut remove_field = false;
                                        if let Some(fv) = field_values.borrow().get(&slug) {
                                            match fv {
                                                ElementFieldValue::Decimal{value} => {
                                                    attr.location.elevation = Some(value.clone());
                                                },
                                                _ => {
                                                    warn!("Unexpected element field type for {}", 
                                                          slug);
                                                }
                                            }
                                            remove_field = true
                                        } else {
                                            attr.location.elevation = None;
                                        }
                                        if remove_field {
                                            field_values.borrow_mut().remove(&slug);
                                        }
                                    },
                                    _ => {
                                    }
                                }
                            }
                        }
                        attr.fields = field_values.borrow().clone();
                        debug!("attr.fields: {:?}", attr.fields);
                    }
                    
                    // TODO: pick more widget values to Command
                    
                    // Pick selected layers
                    {
                        attr.layer_ids = selected_layer_ids.borrow().clone();
                        debug!("attr.layer_ids: {:?}", attr.layer_ids);
                    }

/*
                    // Pick latitude
                    {
                        let entry: gtk::Entry = builder.get_object("latitude")
                                                       .expect("No attraction latitude widget!");
                        if let Some(value_gs) = entry.get_text() {
                            let value: String = value_gs.into();
                            match value.parse::<f64>() {
                                Ok(v) => {
                                    attr.location.lat = v;
                                },
                                Err(e) => {
                                    warn!("Failed to parse latitude: {}", value);
                                }
                            }
                        }
                    }

                    // Pick longitude
                    {
                        let entry: gtk::Entry = builder.get_object("longitude")
                                                       .expect("No attraction longitude widget!");
                        if let Some(value_gs) = entry.get_text() {
                            let value: String = value_gs.into();
                            match value.parse::<f64>() {
                                Ok(v) => {
                                    attr.location.lon = v;
                                },
                                Err(e) => {
                                    warn!("Failed to parse longitude: {}", value);
                                }
                            }
                        }
                    }

                    // Pick altitude
                    {
                        let entry: gtk::Entry = builder.get_object("altitude")
                                                       .expect("No attraction altitude widget!");
                        if let Some(value_gs) = entry.get_text() {
                            let value: String = value_gs.into();


                            let re = Regex::new(r"^\s*(?P<altitude>\S+)\s+(?P<unit>\S+)\s*$").unwrap();
                            if let Some(caps) = re.captures(&value) {
                                if let Some(altitude_number) = caps.name("altitude") {
                                    if let Some(altitude_unit) = caps.name("unit") {
                                        if altitude_unit.as_str() == "m" {
                                            // TODO: regexp parse
                                            match altitude_number.as_str().parse::<f64>() {
                                                Ok(v) => {
                                                    attr.location.elevation = Some(v);
                                                },
                                                Err(e) => {
                                                    warn!("Failed to parse altitude: {}", value);
                                                }
                                            }
                                        } else {
                                            warn!("Unsupported altitude unit: {}", altitude_unit.as_str());
                                        }
                                    } else {
                                        warn!("No altitude unit");
                                    }
                                } else {
                                    warn!("No altitude value");
                                }
                            } else {
                                warn!("Failed to parse altitude: {}", value);
                            }

                        }
                    }
*/                    

                    // Create Command and execute it
                    let cmd = Command::UpdateAttraction{
                        old: orig_attr.borrow().clone(),
                        new: attr,
                    };
                    atlas.execute(&cmd);
                }
                map_win.update_map();

                // Close the window
                attr_win.destroy();
                gtk::Inhibit(true)
            });
        }

        // Show window on top of main window
        map_win.set_transient_of(&win);
        win.show();
    }
}

/// Builds a simple GTK ComboBox showing the given values with the current_value selected.
fn make_simple_combobox(keys: &Vec<String>, values: &Vec<String>, current_key: &String) -> gtk::ComboBox {
    assert!(keys.len() == values.len());

    // Create the widget
    let combo = gtk::ComboBox::new();
    
    // Define data model
    let col_types: [glib::Type; 2] = [
        glib::Type::String,
        glib::Type::String,
    ];
    let store = gtk::ListStore::new(&col_types);
    let mut active_id = String::new();
    let col_indices: [u32; 2] = [0, 1];

    // Fill combo
    for i in 0..values.len() {
        if let Some(key) = keys.get(i) {
            if let Some(value) = values.get(i) {
                let v: [&dyn ToValue; 2] = [
                    &value,
                    &key,
                ];
                store.set(&store.append(), &col_indices, &v);
                
                if key == current_key {
                    active_id = key.clone();
                }
            }
        }
    }

    // Set model
    combo.set_model(Some(&store));
    combo.set_id_column(1);
    combo.set_active_id(Some(active_id.as_str()));

    // Define rendering
    let renderer = gtk::CellRendererText::new();
    combo.pack_start(&renderer, true);
    combo.add_attribute(&renderer, "text", 0);
    
    combo
}

/// Make exlamation label.
fn make_exclamation_label() -> gtk::Label {
    let exclamation_label = gtk::Label::new(Some("!"));
    exclamation_label.set_markup("<span fgcolor=\"#ff0000\"><b>!</b></span>");
    exclamation_label.set_xalign(0.0);
    exclamation_label
}

/// Generates vectors (keys, values) of numbers for month days combo.
fn generate_month_days(n: u8, beginning: bool) -> (Vec<String>, Vec<String>) {
    let mut k = Vec::new();
    let mut v = Vec::new();
    if beginning {
        k.push("".into());
        v.push("beginning".into());
    }
    for i in 1..(n + 1) {
        k.push(format!("{}", i));
        v.push(format!("{}", i));
    }
    if !beginning {
        k.push("".into());
        v.push("end".into());
    }
    (k, v)
}

/// Set the chosen month-day range values.
fn set_month_day_value(fvs: &Rc<RefCell<HashMap<Slug, ElementFieldValue>>>, 
                       slug: &Slug, 
                       field: &ElementField,
                       month_from_: Option<Option<u8>>,
                       day_from_: Option<Option<u8>>,
                       month_to_: Option<Option<u8>>,
                       day_to_: Option<Option<u8>>)
{
    let (month_from, day_from, month_to, day_to) = get_month_day_value(fvs, &slug, &field);

    fvs.borrow_mut().insert(slug.clone(), ElementFieldValue::MonthDay{
        month_from: {
            if let Some(value) = month_from_ {
                value
            } else {
                month_from
            }
        },
        day_from: {
            if let Some(value) = day_from_ {
                value
            } else {
                day_from
            }
        },
        month_to: {
            if let Some(value) = month_to_ {
                value
            } else {
                month_to
            }
        },
        day_to: {
            if let Some(value) = day_to_ {
                value
            } else {
                day_to
            }
        },
    });
}

/// Get month day value from element fields.
fn get_month_day_value(fvs: &Rc<RefCell<HashMap<Slug, ElementFieldValue>>>, 
                       slug: &Slug, 
                       field: &ElementField) -> 
                       (Option<u8>, Option<u8>, Option<u8>, Option<u8>)
{
    if fvs.borrow().contains_key(slug) {
        match fvs.borrow().get(slug).unwrap() {
            ElementFieldValue::MonthDay{
                month_from, day_from, month_to, day_to } =>
            { 
                (month_from.clone(), day_from.clone(), month_to.clone(), day_to.clone())
            },
            _ => {
                warn!("Unexpected ElementFieldValue type for {}", slug);
                (None, None, None, None)
            }
        }
    } else {
        (None, None, None, None)
    }
}

