// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate gtk;
extern crate gio;
extern crate gdk;
extern crate gdk_sys;
extern crate glib;
extern crate cairo;
extern crate chrono;

use std::rc::{Rc};
use std::cell::{Cell, RefCell};
use log::Level::Debug;
use std::time::{Instant, Duration};
use std::collections::{VecDeque};
//use std::collections::{BTreeSet};
use std::process;
use self::gtk::prelude::*;

use gui::mapwindow::{MapWindow};
use gui::floatingtext::*;
use gui::sprite::*;
use gui::textanchor::*;
use gui::cursormode::{CursorMode, CursorKeeper};
use gui::mapcanvasdraw::{ElementDrawers, draw_tiles};
use gui::mapcanvasmenu::{populate_and_show_context_menu};
use core::*;
use tiles::{TileSource};
use geocoord::{Vector, VectorBox, Location, GeoBox, Projection};

// Animation frames per second
const ANIMATION_FPS: f64 = 60.0;

// Scroll history minimum length in elements 
const ANIMATION_SCROLL_HISTORY_LENGTH: usize = 1000;

// Scroll history minimum age in seconds (to start scroll animation)
const ANIMATION_SCROLL_HISTORY_MIN_AGE: f64 = 0.05;

// Minimum pixels per second (to start scroll animation)
const ANIMATION_SCROLL_SPEED_THRESHOLD: f64 = 50.0;

// Maximum pixels per second
const ANIMATION_SCROLL_SPEED_LIMIT: f64 = 2000.0;

// Scroll speed decay ratio per second
const ANIMATION_SCROLL_DECAY: f64 = 0.046;

// Zoom animation duration in seconds.
const ANIMATION_ZOOM_DURATION: f64 = 0.25;

// Visible area margin in pixels. This is needed when gathering elements to be drawn.
const VISIBLE_AREA_MARGIN: f64 = 100.0; // TODO: 600

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum MapCanvasMode {
    /// Waiting for user action passively.
    Void,
    
    /// New element being added to the map.
    Placing,
    
    /// Element being moved on the map.
    Dragging,
    
    /// Map being scrolled on x/y space manually.
    Scrolling,

    /// Inertia scrolling animation.
    ScrollingAnimation,
    
    /// Smooth zooming animation.
    ZoomingAnimation,
}

pub struct MapCanvas {
    /// GTK drawing area widget for the canvas.
    pub widget: Option<gtk::DrawingArea>,
    
    // Cyclic reference to MapWindow.
    map_win: Option<Rc<MapWindow>>,
    
    /// Cursor mode of the canvas.
    pub cursor_keeper: RefCell<CursorKeeper>,
    
    // Floating text elements at southeast corner from bottom to up. 
    // The pivot points are relative to southeast corner of the window and 
    // thus always have negative coordinates.
    float_texts_se: RefCell<Vec<FloatingText>>,

    // Current mode of the canvas.
    mode: Cell<MapCanvasMode>,
    
    // Temporary surface for tile grid
    tile_sprite: RefCell<Option<Sprite>>,
    
    // Mouse location for motion-related events.
    orig_pos: Cell<Vector>,
    orig_center: Cell<Location>,
    prev_motion_wpos: Cell<Vector>,
    
    // Accuracy of the view (degrees per pixel)
    accuracy: Cell<Option<f64>>,
    
    // Scroll position and time history. The positions are mouse positions relative to the 
    // canvas corner.
    scroll_history: RefCell<VecDeque<(Vector, Instant)>>,
    
    // In ScrollingAnimation mode, the speed vector of scrolling.
    scroll_speed_vec: Cell<Vector>,
    
    // Previous animation frame time.
    scroll_prev_time: Cell<Instant>,

    // View center position in pixels to avoid to/from conversions of the center.
    scroll_center_fpos: Cell<Vector>,
    
    // True if zooming, false if zooming out.
    zoom_in: Cell<bool>,
    
    // Start time of the zoom animation
    zoom_start_time: Cell<Instant>,
    
    // Zoom animation zoom factor (0.5 .. 1.0).
    zoom_factor: Cell<f64>,
    
    // Zoom animation factor target value.
    zoom_factor_target: Cell<f64>,

    // Zoom tile surface
    zoom_sprite: RefCell<Option<Sprite>>,
    
    // Mouse location when starting zoom animation
    zoom_mouse_position: Cell<Vector>,
    
    /// Queue for mouse wheel operations. The values are (-1|1, mouse_wpos).
    mouse_wheel_op_queue: RefCell<VecDeque<(i8, Vector)>>,
    
    /// Wrappers for atlass::elements structs.
    element_drawers: RefCell<ElementDrawers>,
    
    /// Popup menu.
    context_menu: RefCell<Option<gtk::Menu>>,
}

impl MapCanvas {
    pub fn new() -> MapCanvas {
        MapCanvas {
            widget: None,
            cursor_keeper: RefCell::new(CursorKeeper::new()),
            float_texts_se: RefCell::new(Vec::new()),
            map_win: None,
            mode: Cell::new(MapCanvasMode::Void),
            tile_sprite: RefCell::new(None),
            orig_pos: Cell::new(Vector::zero()),
            orig_center: Cell::new(Location::new(0.0, 0.0)),
            prev_motion_wpos: Cell::new(Vector::zero()),
            accuracy: Cell::new(None),
            scroll_history: RefCell::new(VecDeque::with_capacity(ANIMATION_SCROLL_HISTORY_LENGTH)),
            scroll_speed_vec: Cell::new(Vector::zero()),
            scroll_prev_time: Cell::new(Instant::now()),
            scroll_center_fpos: Cell::new(Vector::zero()),
            zoom_in: Cell::new(true),
            zoom_start_time: Cell::new(Instant::now()),
            zoom_factor: Cell::new(1.0),
            zoom_factor_target: Cell::new(1.0),
            zoom_sprite: RefCell::new(None),
            zoom_mouse_position: Cell::new(Vector::zero()),
            mouse_wheel_op_queue: RefCell::new(VecDeque::new()),
            element_drawers: RefCell::new(ElementDrawers::new()),
            context_menu: RefCell::new(None),
        }
    }

    pub fn init(&mut self, map_win: Rc<MapWindow>) {
        self.map_win = Some(map_win.clone());
    
        // Create the widget
        let canvas = gtk::DrawingArea::new();
        canvas.set_size_request(512, 512);
        canvas.set_visible(true);
        canvas.set_sensitive(true);
        canvas.set_can_focus(true);

        // Enable the events you wish to get notified about.
        // The 'draw' event is already enabled by the DrawingArea.
        canvas.add_events(gdk::EventMask::BUTTON_PRESS_MASK
                        | gdk::EventMask::BUTTON_RELEASE_MASK
                        | gdk::EventMask::POINTER_MOTION_MASK
                        | gdk::EventMask::SMOOTH_SCROLL_MASK
                        | gdk::EventMask::KEY_PRESS_MASK
                        | gdk::EventMask::KEY_RELEASE_MASK
        );
        
        // Signal handlers
        let mwin = map_win.clone();
        canvas.connect_draw( move |widget, context| { 
            let map_canvas = mwin.map_canvas.borrow();
            map_canvas.draw(context); 
            Inhibit(true) 
        });
        let mwin = map_win.clone();
        canvas.connect_button_press_event( move |widget, event| { 
            let map_canvas = mwin.map_canvas.borrow();
            map_canvas.button_press_event(event); 
            Inhibit(true) 
        } );
        let mwin = map_win.clone();
        canvas.connect_button_release_event( move |widget, event| { 
            let map_canvas = mwin.map_canvas.borrow();
            map_canvas.button_release_event(event); 
            Inhibit(true) 
        } );
        let mwin = map_win.clone();
        canvas.connect_motion_notify_event( move |widget, event| { 
            let map_canvas = mwin.map_canvas.borrow();
            map_canvas.motion_notify_event(event); 
            Inhibit(true) 
        } );
        let mwin = map_win.clone();
        canvas.connect_scroll_event( move |widget, event| { 
            let map_canvas = mwin.map_canvas.borrow();
            map_canvas.scroll_event(event); 
            Inhibit(true) 
        } );
        let mwin = map_win.clone();
        canvas.connect_key_press_event( move |widget, event| { 
            let map_canvas = mwin.map_canvas.borrow();
            Inhibit(map_canvas.key_press_event(event)) 
        } );
                                        
        self.widget = Some(canvas);
    }

    /// Copies copyright texts from the map of the view.
    pub fn redraw_map_meta(&mut self) {
        self.float_texts_se.borrow_mut().clear();

        if let Some(ref map_win) = self.map_win {
            let atlas = map_win.atlas.borrow();
            let view = map_win.map_view.borrow();
            if atlas.maps.contains_key(&view.map_slug) {
                let map = atlas.maps.get(&view.map_slug).unwrap();
            
                let nil_pos = Vector::zero();
                for copyright in &map.copyrights {
                    let mut ft = FloatingText::new(
                            TextAnchor::SouthEast{ margin: 0.0 }, 
                            nil_pos, 
                            copyright.text.clone(), 
                            Some(copyright.url.clone()));
                    ft.font_size = 9.0;
                    ft.padding = 2.0;
                    if map.dark {
                        ft.fg_rgba = (1.0, 1.0, 1.0, 1.0);
                        ft.bg_rgba = (0.0, 0.0, 0.0, 0.3);
                        ft.highlight_rgba = (0.0, 0.0, 1.0, 1.0);
                    } else {
                        ft.fg_rgba = (0.0, 0.0, 0.0, 1.0);
                        ft.bg_rgba = (1.0, 1.0, 1.0, 0.3);
                        ft.highlight_rgba = (0.0, 0.0, 1.0, 1.0);
                    }
                    
                    self.float_texts_se.borrow_mut().push(ft);
                }
            } else {
                warn!("Map not found for slug {}", &view.map_slug);
            }
        } else {
            warn!("No map_win");
        }
    }

    /// Calls 'matching' function if the pixel pos is in the floating text 
    /// and 'non_matching' if not.
    fn map_floating_text<F, G>(&self, pos: Vector, mut matching: F, mut non_matching: G) 
        where F: FnMut(&mut FloatingText), G: FnMut(&mut FloatingText),
    {
        // Iterate southeast texts
        for mut ft in self.float_texts_se.borrow_mut().iter_mut() {
            let contains = {
                if let Some(ref geometry) = ft.geometry {
                    geometry.contains(pos)
                } else {
                    false
                }
            };
            if contains {
                matching(&mut ft);
            } else {
                non_matching(&mut ft);
            }
        }
    }

    /// Signal handler for draw
    fn draw(&self, c: &cairo::Context) {
        let start_time = Instant::now();
        if let Some(ref widget) = self.widget {
            
            // Draw all
            if let Some(ref map_win) = self.map_win {
                let view = map_win.map_view.borrow();
                let atlas = map_win.atlas.borrow();
                if let Some(ref map) = atlas.maps.get(&view.map_slug) {
                    if let Some(tile_source) = map.to_tile_source(&atlas.tokens) {
                        // Call a nested draw
                        self.draw_nested(c, &widget, &map_win, &view, &map, &tile_source);
                    } else {
                        warn!("No tile source for map {}", map.slug);
                    }
                } else {
                    warn!("No map for slug {}", &view.map_slug);
                }
            }
        }

        // Debug draw time    
        if log_enabled!(Debug) { 
            let ms = 1000.0 * duration_to_seconds(&start_time.elapsed());
            if ms >= 15.000 {
                debug!("draw time: {:.3} ms", ms); 
            }
        }

        // Not an ideal place for this, but well...
        if self.mode.get() == MapCanvasMode::Void {
            self.on_void_state();
        }
    }

    /// Draw function with lesser indents
    fn draw_nested(&self, c: &cairo::Context, widget: &gtk::DrawingArea, map_win: &Rc<MapWindow>, 
                  view: &MapView, map: &Map, tile_source: &TileSource) 
    {
        // Default background color
        let background_color = (0.2f64, 0.2f64, 0.2f64);
        // TODO: get_background_color is not available on gtk-rs API yet    
        //  if let Some(style_context) = widget.get_style_context() {
        //      style_context.get_background_color(gtk::StateFlags::STATE_FLAG_NORMAL);
        //  }
        //    

        // Conditional coordinate rounding function
        let round_all_coordinates = { 
            self.mode.get() == MapCanvasMode::Void ||  
            self.mode.get() == MapCanvasMode::Dragging
        };
        let cround = |a: f64| { if round_all_coordinates { a.round() } else { a } };

        // Variables needed for drawing
        let vw = widget.get_allocated_width() as f64;
        let vh = widget.get_allocated_height() as f64;
        let projection = map.as_projection();
        let mut tcache = map_win.tile_cache.borrow_mut();
        let tw = tile_source.tile_width;
        let th = tile_source.tile_height;

        if self.mode.get() == MapCanvasMode::ZoomingAnimation {
            // Prepare tile map for zoom animation
            let mut zoom_sprite_o = self.zoom_sprite.borrow_mut();
            if zoom_sprite_o.is_none() {
                if self.zoom_in.get() {
                    self.prepare_tile_sprite(&mut zoom_sprite_o, vw as i32, vh as i32, tw, th, view.zoom_level - 1);

                    // Trigger the deeper zoom level tile requests
                    let mut tile_sprite_o = self.tile_sprite.borrow_mut();
                    if let Some(ref mut tile_sprite) = *tile_sprite_o {
                        let mut cc = CoordinateContext::with_zoom_level(
                                map_win.clone(), self, view.zoom_level);
                        tile_sprite.zoom_level = Some(view.zoom_level);
                        draw_tiles(tile_sprite, 
                                    cc.loc_to_gpos(view.center), 
                                    cc.loc_to_gpos(view.focus.unwrap_or(view.center)), 
                                    &mut cc, &tile_source, &mut (*tcache));
                    }
                } else {
                    self.prepare_tile_sprite(&mut zoom_sprite_o, vw as i32, vh as i32, tw, th, view.zoom_level);
                }
                
                // Draw onto tile sprite (which is needed to avoid seams during scrolling)
                if let Some(ref mut zoom_sprite) = *zoom_sprite_o {
                    let mut cc = CoordinateContext::with_zoom_level(
                            map_win.clone(), self, zoom_sprite.zoom_level.unwrap());
                    draw_tiles(zoom_sprite, 
                                cc.loc_to_gpos(view.center), 
                                cc.loc_to_gpos(view.focus.unwrap_or(view.center)), 
                                &mut cc, &tile_source, &mut (*tcache));
                }
            }

            // Zoom factor
            let zoom_factor = {
                if self.zoom_in.get() {
                    self.zoom_factor.get()
                } else {
                    self.zoom_factor.get() * 2.0
                }
            };
            let zmp: Vector = self.zoom_mouse_position.get();
            let tran = -zmp * (zoom_factor - 1.0);
            
            // Draw the zoom sprite onto context                            
            if let Some(ref mut zoom_sprite) = *zoom_sprite_o {
                // Context coordinate transformation
                c.save();
                
                if self.zoom_in.get() {
                    // zoom_factor: 1.0 -> 2.0
                    c.translate(tran.x, tran.y);
                    c.scale(zoom_factor, zoom_factor);

                    let vc = Vector::new(vw / 2.0, vh / 2.0);
                    c.translate(-(vc - zmp).x / 2.0, -(vc - zmp).y / 2.0);
                } else {
                    // zoom_factor: 2.0 -> 1.0
                    c.translate(tran.x, tran.y);
                    c.scale(zoom_factor, zoom_factor);
                }

                // Draw the sprite
                let spos = Vector::new(vw / 2.0, vh / 2.0) - zoom_sprite.offset;
                zoom_sprite.paint_on(c, cround(spos.x), cround(spos.y));

                // Context restore
                c.restore();
            }

/* TODO: pystyisiköhän elementtien zoomauksen hoitamaan pelkästään cc:n kanssa kikkailemalla?
            // Draw elements
            c.save();
            if self.zoom_in.get() {
                // zoom_factor: 1.0 -> 2.0
                c.translate(tran.x, tran.y);
                let vc = Vector::new(vw / 2.0, vh / 2.0);
                c.translate(-(vc - zmp).x / 2.0, -(vc - zmp).y / 2.0);
            } else {
                // zoom_factor: 2.0 -> 1.0
                c.translate(tran.x, tran.y);
            }
            let mut cc = CoordinateContext::with_zoom_level(map_win.clone(), self, view.zoom_level);
            let mut drawers = self.element_drawers.borrow_mut();
            drawers.draw_all(&c, &map_win.atlas.borrow(), &mut cc);
            c.restore();
*/            
        } else {
            // Draw tile map for the current zoom level
            let mut cc = CoordinateContext::new(map_win.clone(), self);
            let mut tile_sprite_o = self.tile_sprite.borrow_mut();
            self.prepare_tile_sprite(&mut tile_sprite_o, vw as i32, vh as i32, tw, th, view.zoom_level);
            if let Some(ref mut tile_sprite) = *tile_sprite_o {
                // Draw onto tile sprite (which is needed to avoid seams during scrolling)
                draw_tiles(tile_sprite, 
                            cc.loc_to_gpos(view.center), 
                            cc.loc_to_gpos(view.focus.unwrap_or(view.center)), 
                            &mut cc, &tile_source, &mut (*tcache));
                    
                // Draw the tile sprite onto context                            
                let wpos = Vector::new(vw / 2.0, vh / 2.0) - tile_sprite.offset;
                tile_sprite.paint_on(c, cround(wpos.x), cround(wpos.y));
            }

            // Draw elements
            let mut cc = CoordinateContext::with_zoom_level(map_win.clone(), self, view.zoom_level);
            let mut drawers = self.element_drawers.borrow_mut();
            drawers.draw_all(&c, &map_win.atlas.borrow(), &mut cc, 
                             view.zoom_level, map.dark, round_all_coordinates);
        }

        // Draw copyright texts
        let margin = 2.0;
        let mut ty = -margin;
        for float_text in self.float_texts_se.borrow_mut().iter_mut() {
            // Draw the text
            float_text.pivot = Vector::new(-float_text.padding - margin, ty);
            float_text.draw(c, Vector::new(vw as f64, vh as f64), |a| { a.round() });
            ty -= float_text.font_size + 2.0 * float_text.padding + margin;
        }
    }

    /// Ensure that the sprite is created and has wanted dimensions and zoom_level.
    fn prepare_tile_sprite(&self, sprite_o: &mut Option<Sprite>, vw: i32, vh: i32, tw: i32, th: i32, zoom_level: u8) {
        // Width needs some extra because of:
        // 1) view dimension flooring
        // 2) tile alignment related to scrolling
        // 3) tile delta within sprite
        let tsw = (vw / tw + 3) * tw;
        let tsh = (vh / th + 3) * th;
        if {
            // Reallocate the sprite if the dimensions don't match or if it doesn't 
            // exists yet. Otherwise, ensure that zoom_level matches with the view.
            if let Some(ref mut sprite) = *sprite_o {
                if sprite.width() != tsw || sprite.width() != tsh {
                    true
                } else {
                    sprite.zoom_level = Some(zoom_level);
                    false
                } 
            } else {
                true
            }
        } {
            if let Ok(sprite) = Sprite::with_offset(
                                      tsw, 
                                      tsh,
                                      Vector::zero(),
                                      Some(zoom_level), false) 
            {
                *sprite_o = Some(sprite);
            } else {
                debug!("prepare_tile_sprite: Failed to create sprite vw={:?} vh={:?} tw={:?} th={:?} zoom_level={:?}", 
                       vw, vh, tw, th, zoom_level);
            }
        }
    }

    /// Event handler for keyboard press events.
    fn key_press_event(&self, ev: &gdk::EventKey) -> bool {
        let keyval = ev.get_keyval();
        debug!("key_press_event: event.keyval={:?}", keyval);
        
	    if keyval == gdk::enums::key::plus || keyval == gdk::enums::key::KP_Add {
	        // Zoom in
            let mouse_wpos = self.prev_motion_wpos.get();
            self.mouse_wheel_op_queue.borrow_mut().push_back((1, mouse_wpos));
	    } else if keyval == gdk::enums::key::minus || keyval == gdk::enums::key::KP_Subtract {
	        // Zoom out
            let mouse_wpos = self.prev_motion_wpos.get();
            self.mouse_wheel_op_queue.borrow_mut().push_back((-1, mouse_wpos));
        } else if keyval == gdk::enums::key::Tab {
            // Cycle between two maps
            if let Some(ref map_win) = self.map_win {
                let mut view = map_win.map_view.borrow_mut();
                
                // Swap the current map and the previous one
                let t = view.map_slug.clone();
                view.map_slug = view.prev_map_slug.clone().unwrap_or(t.clone());
                view.prev_map_slug = Some(t);
            
                // Update map canvas with the selected map
                map_win.redraw_map();
            }
        } else if keyval == gdk::enums::key::KP_Left {
            // TODO
	    } else {
	        // Map canvas doesn't process this key event
            return false
        }
        
        // Trigger op queue handling
        if self.mode.get() == MapCanvasMode::Void {
            self.on_void_state();
        }
        true
    }

    /// Event handler for mouse button press. Either start dragging a map element or scrolling the 
    /// map. This doesn't select map element (to avoid accidental drag instead of scroll).
    fn button_press_event(&self, ev: &gdk::EventButton) {
        let pos = Vector::with_tuple(ev.get_position());
        debug!("button_press_event: {:?}", pos);
        
        // Check whether the click is on a map element hotspot or not
        if let Some(ref map_win) = self.map_win {
            // The default mode is scrolling
            let mut new_mode = MapCanvasMode::Scrolling; // false warning
            self.scroll_history.borrow_mut().clear();
            self.orig_pos.set(pos);
            self.orig_center.set(map_win.map_view.borrow().center);
            
            // Match mode
            {
                let mut drawers = self.element_drawers.borrow_mut();
                match self.mode.get() {
                    MapCanvasMode::Void => {
                        if drawers.hotspot_ready_for_drag() {
                            // Drag the current map element
                            drawers.start_drag();
                            new_mode = MapCanvasMode::Dragging;
                        } else if drawers.hover_hotspot.is_some() {
                            // Select the map element
                            let mut cc = CoordinateContext::new(map_win.clone(), self);
                            drawers.touch_or_hover(&cc.wpos_to_gpos(pos), true);
                            
                            // Update map
                            map_win.redraw_map();
                            new_mode = MapCanvasMode::Scrolling;
                        } else {
                            // Deselect the element
                            let mut cc = CoordinateContext::new(map_win.clone(), self);
                            drawers.touch_or_hover(&cc.wpos_to_gpos(pos), true);
                        
                            // Start scrolling
                            new_mode = MapCanvasMode::Scrolling;
                        } 
                    }
                    _ => {
                        new_mode = MapCanvasMode::Scrolling;
                    }
                }
            }

            // If the button is menu button...
            if ev.get_button() == 3 {
                if self.context_menu.borrow().is_none() {
                    *self.context_menu.borrow_mut() = Some(gtk::Menu::new());
                }
                if let Some(ref mut context_menu) = *self.context_menu.borrow_mut() {
                    let drawers = self.element_drawers.borrow_mut();
                    let mut cc = CoordinateContext::new(map_win.clone(), self);
                    populate_and_show_context_menu(context_menu, 
                                                   &ev, 
                                                   drawers.selected_hotspot.clone(), 
                                                   map_win.clone(),
                                                   &mut cc);
                }
                new_mode = MapCanvasMode::Void;
            }
        
            self.mode.set(new_mode);
        }

        // Cursor shape update if needed
        self.update_cursor_mode();
    }

    /// Event handler for mouse button release. Either stop drag or scroll, or select a new 
    /// map element.
    fn button_release_event(&self, ev: &gdk::EventButton) {
        if let Some(ref map_win) = self.map_win {
            let pos = Vector::with_tuple(ev.get_position());
            debug!("button_release_event: {:?} mode={:?}", pos, self.mode.get());
            
            // Either end the drag, scrolling or just keep the selection
            let mut new_mode = MapCanvasMode::Void;
            match self.mode.get() {
                MapCanvasMode::Dragging => {
                    let mut cc = CoordinateContext::new(map_win.clone(), self);
                    let mut drawers = self.element_drawers.borrow_mut();
                    let mut atlas = map_win.atlas.borrow_mut();
                    drawers.end_drag(&mut atlas);

                    if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                        let view = map_win.map_view.borrow_mut();
                        drawers.gather_visible(&area, view.zoom_level, &atlas, &mut cc);
                        drawers.gather_hotspots(&area, &atlas, &mut cc);
                    }
                },
                MapCanvasMode::Scrolling => {
                    let mut scroll_history = self.scroll_history.borrow_mut();
                    let history_size = scroll_history.len();
                    
                    // Reference point 0 from the current measurements
                    let pos1 = pos;
                    let time1 = Instant::now();
                    
                    // Reference point 1 from far enough in the scroll history
                    let (mut pos0, mut time0) = (pos1, time1);
                    while duration_to_seconds(&(time1 - time0)) < ANIMATION_SCROLL_HISTORY_MIN_AGE {
                        if let Some((pos, time)) = scroll_history.pop_back() {
                            pos0 = pos; time0 = time; 
                        } else {
                            break;
                        }
                    }
                    
                    if duration_to_seconds(&(time1 - time0)) >= ANIMATION_SCROLL_HISTORY_MIN_AGE {
                        // Calculate a speed vector
                        let mut cc = CoordinateContext::new(map_win.clone(), self);
                        let delta_pos = pos1 - pos0;
                        let delta_time = duration_to_seconds(&(time1 - time0));
                        assert!(delta_time > 0.0);
                        let mut speed_vec = Vector::new(-delta_pos.x as f64 / delta_time, 
                                                    -delta_pos.y as f64 / delta_time );
                        // Speed limit
                        if speed_vec.length() > ANIMATION_SCROLL_SPEED_LIMIT {
                            speed_vec = speed_vec * (ANIMATION_SCROLL_SPEED_LIMIT / speed_vec.length());
                        }

                        // Start animation if speed threshold is reached
                        if speed_vec.length() >= ANIMATION_SCROLL_SPEED_THRESHOLD {
                            new_mode = MapCanvasMode::ScrollingAnimation;
                            {
                                let center_pos = cc.loc_to_gpos(map_win.map_view.borrow().center);
                                self.scroll_speed_vec.set(speed_vec);
                                self.scroll_prev_time.set(time1);
                                self.scroll_center_fpos.set(center_pos.into());
                            }

                            // GTK timeout closure
                            let map_win_r = map_win.clone();
                            let decay = ANIMATION_SCROLL_DECAY.powf(1.0 / ANIMATION_FPS);
                            timeout_add((1000.0 / ANIMATION_FPS) as u32, move || {
                                let map_canvas = map_win_r.map_canvas.borrow();
                                let mut cc = CoordinateContext::new(map_win_r.clone(), &map_canvas);
                        
                                // If mode has changed, stop scrolling
                                if map_canvas.mode.get() != MapCanvasMode::ScrollingAnimation {
                                    map_win_r.redraw_map();
                                    return Continue(false);
                                }
                            
                                // Get needed parameters
                                let now = Instant::now();
                                let delta_time = duration_to_seconds(
                                                 &(now - map_canvas.scroll_prev_time.get()));
                                let mut speed_vec = map_canvas.scroll_speed_vec.get();
                                let mut center_fpos = map_canvas.scroll_center_fpos.get();
                                
                                // Reduce speed vector
                                let f: f64 = decay;
                                assert!(f < 1.0);
                                speed_vec = speed_vec * f.max(0.0);
                                map_canvas.scroll_speed_vec.set(speed_vec);
                                let speed_vec_step = speed_vec * delta_time;
                                
                                // Stop if the speed is too low
                                if speed_vec_step.length2() < 0.1 {
                                    map_canvas.mode.set(MapCanvasMode::Void);

                                    // Update drawers
                                    if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                                        let view = map_win_r.map_view.borrow_mut();
                                        let mut drawers = map_canvas.element_drawers.borrow_mut();
                                        let atlas = map_win_r.atlas.borrow();
                                        drawers.gather_visible(&area, view.zoom_level, 
                                                               &atlas, &mut cc);
                                        drawers.gather_hotspots(&area, &atlas, &mut cc);
                                    }

                                    map_win_r.redraw_map();
                                    map_win_r.tile_cache.borrow_mut().check_cache(false);

                                    return Continue(false);
                                }

                                // Move view center
                                center_fpos = center_fpos + speed_vec_step;
                                map_canvas.scroll_center_fpos.set(center_fpos);
                                {
                                    let new_center_loc = cc.gpos_to_loc(center_fpos);
                                    let mut view = map_win_r.map_view.borrow_mut();
                                    view.center = new_center_loc;
                                    view.focus = Some(new_center_loc);
                                }
                                    
                                // Update coordinates on the bottom bar
                                let new_focus_loc = cc.wpos_to_loc(pos);
                                map_win_r.update_coordinates_button(Some(new_focus_loc), 
                                                                    map_canvas.accuracy.get());

                                // Update drawers: TODO: how does this affect to performance?
                                if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                                    let view = map_win_r.map_view.borrow_mut();
                                    let mut drawers = map_canvas.element_drawers.borrow_mut();
                                    let atlas = map_win_r.atlas.borrow();
                                    drawers.gather_visible(&area, view.zoom_level, &atlas, &mut cc);
                                    drawers.gather_hotspots(&area, &atlas, &mut cc);
                                }
                                
                                // Request a map update    
                                map_win_r.redraw_map();

                                // Save time
                                map_canvas.scroll_prev_time.set(now);
                                
                                Continue(true)
                            });
                        }
                    }
                },
                _ => {
                }
            }
            self.mode.set(new_mode);
                
            // Open a url if one of the floating texts is clicked.
            let url: RefCell<Option<String>> = RefCell::new(None);
            self.map_floating_text(pos, 
                { |ft| { *url.borrow_mut() = ft.url.clone(); } }, 
                { |ft| { } }) ;
            if let Some(ref url) = url.into_inner() {
                info!("Opening url: {}", url);
                match process::Command::new(&settings_read().browser_command).arg(url).spawn() {
                    Ok(child) => { }
                    Err(e) => {
                        error!("Failed to open the url: {}", e);
                    }
                }
            }
            
            // Request map refresh to get coordinate rounding on
            map_win.redraw_map();
        } else {
            self.mode.set(MapCanvasMode::Void);
        }

        // Cursor shape update if needed
        self.update_cursor_mode();
    }

    /// Drop off all drawers. This is typically called after map is changed.
    /// After this method gather_drawers should be called.
    pub fn drop_drawers(&self) {
        {
            let mut drawers = self.element_drawers.borrow_mut();
            drawers.drop_all();  
        }
    }

    /// Gather map elements for the view. This is typically called at same context as
    /// MapWin::redraw_map when elements are either added or removed.
    pub fn gather_drawers(&self) {
        if let Some(ref map_win) = self.map_win {
            let mut cc = CoordinateContext::new(map_win.clone(), self);
            let zoom_level = map_win.map_view.borrow().zoom_level;
            let mut drawers = self.element_drawers.borrow_mut();
            let atlas = map_win.atlas.borrow();
            if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                drawers.gather_visible(&area, zoom_level, &atlas, &mut cc);
                drawers.gather_hotspots(&area, &atlas, &mut cc);
            }
        }
    }

    /// Event handler for mouse motion. Either drag, scroll or hover.
    fn motion_notify_event(&self, ev: &gdk::EventMotion) {
        //debug!("motion_notify_event: mode={:?}", *self.mode.borrow());
        if let Some(ref map_win) = self.map_win {
            let mut cc = CoordinateContext::new(map_win.clone(), self);
            let redraw_map = Cell::new(false);
            let pos = Vector::with_tuple(ev.get_position());
            match self.mode.get() {
                MapCanvasMode::Void => {
                    // Check for possible hover over element
                    let mut drawers = self.element_drawers.borrow_mut();
                    if drawers.touch_or_hover(&cc.wpos_to_gpos(pos), false) {
                        redraw_map.set(true);
                    }
                
                    // Check for possible hover highlight
                    self.map_floating_text(pos, 
                        { |ft| {
                            if !ft.highlight && ft.url.is_some() {
                                ft.highlight = true;
                                redraw_map.set(true);
                            }
                        } }, 
                        { |ft| {
                            if ft.highlight {
                                ft.highlight = false;
                                redraw_map.set(true);
                            }
                        } }) ;
                        
                    // Save the position for a possible key event
                    self.prev_motion_wpos.set(pos);
                },
                MapCanvasMode::Placing => {
                },
                MapCanvasMode::Dragging => {
                    let mut drawers = self.element_drawers.borrow_mut();
                    let mut atlas = map_win.atlas.borrow_mut();
                    drawers.drag(&cc.wpos_to_gpos(pos), 
                                 &mut atlas, &mut cc);
                    redraw_map.set(true);
                },
                MapCanvasMode::Scrolling => {
                    // Compute delta
                    let orig_pos = self.orig_pos.get();
                    let delta_pos = pos - orig_pos;
                    
                    if !delta_pos.is_zero() {
                        // Move center of the view
                        let orig_center_pos = cc.loc_to_wpos(self.orig_center.get());
                        let new_center = cc.wpos_to_loc(orig_center_pos - delta_pos);
                        map_win.map_view.borrow_mut().center = new_center;

                        // Request a map update                        
                        redraw_map.set(true);
                        
                        // Add pos and time to history for inertia
                        let mut scroll_history = self.scroll_history.borrow_mut();
                        if scroll_history.len() >= ANIMATION_SCROLL_HISTORY_LENGTH {
                            scroll_history.pop_front();
                        }
                        scroll_history.push_back((pos, Instant::now()));
                    }
                    
                    // Check if elements need to be re-gathered
                    let mut drawers = self.element_drawers.borrow_mut();
                    let atlas = map_win.atlas.borrow_mut();
                    if let Some(visible_area) = cc.calculate_visible_area(0.0) {
                        if drawers.gather_needed(&visible_area) {
                            if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                                let view = map_win.map_view.borrow_mut();
                                drawers.gather_visible(&area, view.zoom_level, &atlas, &mut cc);
                                drawers.gather_hotspots(&area, &atlas, &mut cc);
                            }
                        }
                    }
                }
                _ => { }
            }
            
            // Update coordinates label
            {
                let focus = cc.wpos_to_loc(pos);
                map_win.map_view.borrow_mut().focus = Some(focus);
                map_win.update_coordinates_button(Some(focus), self.accuracy.get());
            }
                    

            // Request map update if needed.
            if redraw_map.get() == true {
                map_win.redraw_map();
            }
        }

        // Cursor shape update if needed
        self.update_cursor_mode();
    }

    /// Event handler for mouse wheel.
    fn scroll_event(&self, ev: &gdk::EventScroll) {
        if let Some(ref map_win) = self.map_win {
            let mouse_wpos = Vector::with_tuple(ev.get_position());

            if let Some(ref widget) = self.widget {
                // Zoom direction
                let (dx, dy) = ev.get_delta();
                if dy < 0.0 {
                    // Zoom in
                    self.mouse_wheel_op_queue.borrow_mut().push_back((1, mouse_wpos));
                } else if dy > 0.0 {
                    // Zoom out
                    self.mouse_wheel_op_queue.borrow_mut().push_back((-1, mouse_wpos));
                }
            }
        }
        
        if self.mode.get() == MapCanvasMode::Void {
            self.on_void_state();
        }
    }
    
    /// Called after canvas state has been transferred to Void.
    fn on_void_state(&self) {
        if let Some(ref map_win) = self.map_win {
            let mut cc = CoordinateContext::new(map_win.clone(), self);
            let mut map_view = map_win.map_view.borrow_mut();
            let center_wpos = cc.loc_to_wpos(map_view.center);
            
            // Check mouse wheel queue
            let mut op_queue = self.mouse_wheel_op_queue.borrow_mut();
            if let Some((ref op, ref mouse_wpos)) = op_queue.pop_front() {
                let mut zoom_op = 0i8;
                match *op {
                    1 => {
                        // Max zoom level
                        let max_zoom_level = {
                            if let Some(ref map) = map_win.atlas.borrow().maps.get(&map_view.map_slug) {
                                map.max_zoom_level
                            } else {
                                16
                            }
                        };
                    
                        // Zoom in
                        if map_view.zoom_level < max_zoom_level {
                            map_view.focus = Some(cc.wpos_to_loc(*mouse_wpos));
                            let new_center_pos = mouse_wpos.weighted_average(center_wpos, 0.5);
                            map_view.center = cc.wpos_to_loc(new_center_pos);
                            map_view.zoom_level += 1;
                            zoom_op = 1;
                        }
                    },
                    -1 => {
                        // Zoom out
                        if map_view.zoom_level >= 3 {
                            map_view.focus = Some(cc.wpos_to_loc(*mouse_wpos));
                            let new_center_pos = mouse_wpos.weighted_average(center_wpos, 2.0);
                            map_view.center = cc.wpos_to_loc(new_center_pos);
                            map_view.zoom_level -= 1;                
                            zoom_op = -1;
                        }
                    }
                    _ => {
                        warn!("Unrecognized mouse wheel op: {}", *op);
                    }
                }
                
                if zoom_op != 0 {
                    // Let cache know that we changed the level.
                    {
                        let mut tcache = map_win.tile_cache.borrow_mut();
                        tcache.focus_on_zoom_level(map_view.zoom_level);
                    }
                    
                    // GTK timeout closure for the zoom animation
                    self.mode.set(MapCanvasMode::ZoomingAnimation);
                    self.zoom_in.set( { zoom_op == 1 } );
                    self.zoom_factor.set(1.0);
                    self.zoom_factor_target.set( match zoom_op { -1 => 0.5, 1 => 2.0, _ => {1.0} } );
                    self.zoom_start_time.set(Instant::now());
                    *self.zoom_sprite.borrow_mut() = None;
                    self.zoom_mouse_position.set(*mouse_wpos);
                    let map_win_r = map_win.clone();
                    timeout_add((1000.0 / ANIMATION_FPS) as u32, move || {
                        let map_canvas = map_win_r.map_canvas.borrow();
                
                        // If mode has changed, stop zooming
                        if map_canvas.mode.get() != MapCanvasMode::ZoomingAnimation {
                
                            // Gather elements for the view            
                            let view = map_win_r.map_view.borrow();
                            let mut cc = CoordinateContext::with_zoom_level(
                                    map_win_r.clone(), &map_canvas, view.zoom_level);
                            if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                                let atlas = map_win_r.atlas.borrow();
                                let mut drawers = map_canvas.element_drawers.borrow_mut();
                                drawers.gather_visible(&area, view.zoom_level, &atlas, &mut cc);
                                drawers.gather_hotspots(&area, &atlas, &mut cc);
                            }

                            // Update map
                            map_win_r.redraw_map();
                            *map_canvas.zoom_sprite.borrow_mut() = None;
                            
                            return Continue(false);
                        }
                        
                        // The current factor
                        let mut zoom_factor = map_canvas.zoom_factor.get();
                        let zoom_factor_target = map_canvas.zoom_factor_target.get();
                        let elapsed = duration_to_seconds(&map_canvas.zoom_start_time.get().elapsed());
                        let expected_duration = {
                            if map_canvas.mouse_wheel_op_queue.borrow().len() > 0 {
                                ANIMATION_ZOOM_DURATION / 4.0
                            } else {
                                ANIMATION_ZOOM_DURATION
                            }
                        };
                        let remaining_time = expected_duration - elapsed;
                        let remaining_ticks = ANIMATION_FPS * remaining_time;
                        
                        // Zoom in/out
                        if remaining_ticks > 0.0 {
                            let zoom_factor_step = (zoom_factor_target - zoom_factor) / remaining_ticks;
                            debug!(" zoom_factor={:.2} step={:.3} ticks={:.1} time={:.3}", 
                                zoom_factor, zoom_factor_step, remaining_ticks, remaining_time);
                            zoom_factor = zoom_factor + zoom_factor_step;
                            map_canvas.zoom_factor.set(zoom_factor)
                        }
                        
                        // Stop if zooming is ready
                        if zoom_factor <= 0.5 || zoom_factor >= 2.0 || remaining_ticks <= 0.0 {
                            map_canvas.mode.set(MapCanvasMode::Void);
                            zoom_factor = 1.0;
                            *map_canvas.zoom_sprite.borrow_mut() = None;
                            map_win_r.redraw_map();
                            map_canvas.zoom_factor.set(zoom_factor);
                            return Continue(false);
                        }

                        // Request update
                        map_win_r.redraw_map();
                        Continue(true)
                    });
                
                    // Request map update
                    map_win.redraw_map();
                    map_win.update_zoom_level_label(map_view.zoom_level);
                }
            }

            // Flush tile cache if needed
            let map_canvas = map_win.map_canvas.borrow();
            if map_canvas.mode.get() == MapCanvasMode::Void {
                map_win.tile_cache.borrow_mut().check_cache(false);
            }
            
            // Make element drawers from elements in atlas
            if let Some(area) = cc.calculate_visible_area(VISIBLE_AREA_MARGIN) {
                let atlas = map_win.atlas.borrow();
                let mut drawers = self.element_drawers.borrow_mut();
                drawers.gather_visible(&area, map_view.zoom_level, &atlas, &mut cc);
                drawers.gather_hotspots(&area, &atlas, &mut cc);
            }
        }

        // Cursor shape update if needed
        self.update_cursor_mode();
    }
    
    /// Changes cursor shape based on canvas mode if needed.
    fn update_cursor_mode(&self) {
        if let Some(ref canvas) = self.widget {
            let cursor_mode = match self.mode.get() {
                MapCanvasMode::Void               => { CursorMode::Draggable }
                MapCanvasMode::Placing            => { CursorMode::Target }
                MapCanvasMode::Dragging           => { CursorMode::Target }
                MapCanvasMode::Scrolling          => { CursorMode::Dragging }
                MapCanvasMode::ScrollingAnimation => { CursorMode::Draggable }
                MapCanvasMode::ZoomingAnimation   => { CursorMode::Draggable }
            };
            self.cursor_keeper.borrow_mut().change_for(cursor_mode, canvas.get_window());
        }
    
    }
}

// -------------------------------------------------------------------------------------------------

/// A helper class for coordinate transformations. This class is meant to be in effect 
/// temporarily only as it takes a snapshot of needed fields.
#[derive(Clone, Debug)]
pub struct CoordinateContext {
    projection: Projection,
    center: Location,
    ppdoe: f64,
    tile_width: i64,
    canvas_width: f64,
    canvas_height: f64,
    global_nw_pos: Vector,
}

impl CoordinateContext {
    /// Construct a new context using information from map window and drawing area widget.
    pub fn new(map_win: Rc<MapWindow>, map_canvas: &MapCanvas) -> CoordinateContext {
        let zoom_level = map_win.map_view.borrow().zoom_level;
        CoordinateContext::with_zoom_level(map_win, map_canvas, zoom_level)
    }

    /// Construct a new context using information from map window and drawing area widget
    /// but use a different zoom level.
    pub fn with_zoom_level(map_win: Rc<MapWindow>, map_canvas: &MapCanvas, zoom_level: u8) -> CoordinateContext {
        let map_view = map_win.map_view.borrow();
        if let Some(ref map) = map_win.atlas.borrow().maps.get(&map_view.map_slug) {
            if let Some(tw) = map.tile_width {
                if let Some(ref widget) = map_canvas.widget {
                    let mut cc = CoordinateContext {
                        projection: map.as_projection(),
                        center: map_view.center,
                        ppdoe: ((tw as u64) << (zoom_level as u64)) as f64 / 360.0,
                        tile_width: tw as i64,
                        canvas_width: widget.get_allocated_width() as f64,
                        canvas_height: widget.get_allocated_height() as f64,
                        global_nw_pos: Vector::zero(),
                    };
                    cc.global_nw_pos = cc.projection.northwest_global_pixel(cc.ppdoe);            
                    return cc;
                }
            }
        }
        panic!("CoordinateContext creation failed!");
    }
    
    /// Convert location to local pixel position.
    pub fn loc_to_wpos(&mut self, loc: Location) -> Vector {
        let center_pos = self.projection.location_to_global_pixel_pos(self.center, self.ppdoe);
        let view_nw_pos = center_pos - Vector::new(self.canvas_width / 2.0, self.canvas_height / 2.0);
        let global_width = self.ppdoe * 360.0;
        
        let global_pos = self.projection.location_to_global_pixel_pos(loc, self.ppdoe);
        let mut wpos = global_pos - view_nw_pos;
        while wpos.x < 0.0 {
            wpos.x += global_width;
        }
        while wpos.x >= global_width {
            wpos.x -= global_width;
        }
        wpos
    }

    /// Convert local pixel position to location.    
    pub fn wpos_to_loc(&mut self, local_pos: Vector) -> Location {
        let center_pos = self.projection.location_to_global_pixel_pos(self.center, self.ppdoe);
        let view_nw_pos = center_pos - Vector::new(self.canvas_width / 2.0, self.canvas_height / 2.0);
        
        let global_pos = view_nw_pos + local_pos;            
        self.projection.global_pixel_pos_to_location(global_pos, self.ppdoe)
    }
    
    /// Convert location to global pixel position.
    pub fn loc_to_gpos(&mut self, loc: Location) -> Vector {
        self.projection.location_to_global_pixel_pos(loc, self.ppdoe)
    }

    /// Convert local pixel position to global pixel position.
    pub fn wpos_to_gpos(&mut self, local_pos: Vector) -> Vector {
        // TODO: optimize
        let loc = self.wpos_to_loc(local_pos);
        self.loc_to_gpos(loc)
    }
    
    /// Convert global pixel position to location.
    pub fn gpos_to_loc(&mut self, global_pos: Vector) -> Location {
        self.projection.global_pixel_pos_to_location(global_pos, self.ppdoe)
    }

    /// Return the global most northwest pixel position of the current zoom level.
    pub fn global_northwest_pos(&self) -> Vector {
        self.global_nw_pos
    }
    
    /// Converts GeoBox (loc) to VectorBox (gpos) in the current context.
    pub fn gb_to_vb(&mut self, bb: &GeoBox) -> VectorBox {
        VectorBox::new(self.loc_to_gpos(*bb.northwest()), self.loc_to_gpos(*bb.southeast()))
    }

    /// Calculate visible area matching the canvas. Parameter `margin` is given in pixels and 
    /// should be 0.0 if no margins are wanted. 
    pub fn calculate_visible_area(&self, margin: f64) -> Option<GeoBox> {
        if self.canvas_width >= 1.0 && self.canvas_height >= 1.0 {
            if (self.canvas_width + margin * 2.0) / self.ppdoe < 360.0 {
                let center_pos = self.projection.location_to_global_pixel_pos(self.center, self.ppdoe);
                let view_nw_pos = center_pos - Vector::new(self.canvas_width / 2.0 + margin, 
                                                           self.canvas_height / 2.0 + margin);
                let view_se_pos = center_pos + Vector::new(self.canvas_width / 2.0 + margin, 
                                                           self.canvas_height / 2.0 + margin);
                
                Some(GeoBox::new(self.projection.global_pixel_pos_to_location(view_nw_pos, self.ppdoe),
                                 self.projection.global_pixel_pos_to_location(view_se_pos, self.ppdoe)))
            } else {
                Some(GeoBox::new(Location::new(90.0, -179.9999999999), Location::new(-90.0, 180.0)))
            }
        } else {
            None
        }
    }
}

// ------------------------------------------------------------------------------------------------

/// Convert duration to milliseconds
fn duration_to_seconds(i: &Duration) -> f64 {
    let secs = i.as_secs() as f64;
    let nsecs = i.subsec_nanos() as f64;
    (secs + 0.000000001 * nsecs)
}

// ------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_coordinate_context() {
        {
            // CoordinateContext with center 10° west from 180th meridian
            let tw = 256;
            let zoom_level = 2.0;
            let ppdoe = ((tw as u64) << (zoom_level as u64)) as f64 / 360.0;
            let mut projection = Projection::new_mercator_projection();
            let gnwpos = projection.northwest_global_pixel(ppdoe);
            let mut cc = CoordinateContext {
                projection: projection,
                center: Location::new(0.0, 170.0),
                ppdoe: ppdoe,
                tile_width: tw as i64,
                canvas_width: 800.0,
                canvas_height: 800.0,
                global_nw_pos: gnwpos,
            };

            // Location 5° west from 180th meridian
            let loc = Location::new(0.0, 175.0);
            let wpos = cc.loc_to_wpos(loc);
            assert!(wpos.x > 0.0);
            assert!(wpos.x < 800.0);

            // Location 5° east from 180th meridian
            let loc = Location::new(0.0, -175.0);
            let wpos = cc.loc_to_wpos(loc);
            assert!(wpos.x > 0.0);
            assert!(wpos.x < 800.0);
        }
        
        {
            // CoordinateContext with center 10° east from 180th meridian
            let tw = 256;
            let zoom_level = 2.0;
            let ppdoe = ((tw as u64) << (zoom_level as u64)) as f64 / 360.0;
            let mut projection = Projection::new_mercator_projection();
            let gnwpos = projection.northwest_global_pixel(ppdoe);
            let mut cc = CoordinateContext {
                projection: projection,
                center: Location::new(0.0, -170.0),
                ppdoe: ppdoe,
                tile_width: tw as i64,
                canvas_width: 800.0,
                canvas_height: 800.0,
                global_nw_pos: gnwpos,
            };

            // Location 5° west from 180th meridian
            let loc = Location::new(0.0, 175.0);
            let wpos = cc.loc_to_wpos(loc);
            assert!(wpos.x > 0.0);
            assert!(wpos.x < 800.0);

            // Location 5° east from 180th meridian
            let loc = Location::new(0.0, -175.0);
            let wpos = cc.loc_to_wpos(loc);
            assert!(wpos.x > 0.0);
            assert!(wpos.x < 800.0);
        }
    }    
}

