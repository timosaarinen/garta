// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::{HashSet};

use core::{Atlas, UniqueId, MapElement, Hotspot, ColorName};
use geocoord;
use geocoord::geo::{GeoBox, Location};

// ---- Waypoint -------------------------------------------------------------------------------

/// GPX waypoint
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Waypoint { 
    id: UniqueId,
    location: Location,
}

impl Waypoint {
    /// Constructor.
    pub fn new(id: UniqueId, loc: Location) -> Waypoint {
        Waypoint {        
            id: id,
            location: loc,
        }
    }
}

impl MapElement for Waypoint {
    fn id(&self) -> UniqueId { self.id }

    fn bounding_box(&self) -> GeoBox {
        GeoBox::new(self.location, self.location)
    }
    
    fn gather_hotspots(&self, area: &GeoBox, hotspot_set: &mut HashSet<Hotspot>) { 
        if area.contains(&self.location) {
            hotspot_set.insert(Hotspot::new(self.id, self.location, None));
        }
    }

    fn get_color_name(&self, atlas: &Atlas) -> ColorName {
        atlas.default_attraction_color.clone() // TODO
    }
}

// ---- Path ---------------------------------------------------------------------------------------

/// GPX routes and tracks.
pub enum PathMode {
    Neither,
    PathTrack { track: geocoord::gpx::model::Track },
    PathRoute { route: geocoord::gpx::model::Route },
}

pub struct Path {
    id: UniqueId,
    mode: PathMode,
}

impl Path {
    /// Create a new empty layer.
    pub fn new(id: UniqueId, slug: String) -> Path {
        Path{
            id: id,
            mode: PathMode::Neither,
        }    
    }
}

impl Path {
    /// Remove idle points from the beginning and end of the path.
    pub fn trim(&mut self, radius: f64) {
    }
    
    /// Remove points that have too high acceleration (or decceleration).
    pub fn limit_acceleration(&mut self, max_acceleration: f64) {
    }
    
    /// Find idle spots on the track and split it to legs when found.
    pub fn divide_on_idle(&mut self, radius: f64, delay: f64) {
    }

    /// Join legs if their end and start time is lesser than the given.
    pub fn join_legs(&mut self, max_time: f64) {
    }

    /// drop points that make the track too sharp.    
    pub fn smooth(&mut self, max_angle: f64) {
    }
    
    /// Drop points to make the tracke sparser.
    pub fn make_sparser(&mut self, min_distance: f64) {
//        for leg in self.legs.iter_mut() {
//            for point in leg.borrow().points.iter() {
//                // TODO
//            }
//        }
    }
}

impl MapElement for Path {
    fn id(&self) -> UniqueId { self.id }
 
    fn bounding_box(&self) -> GeoBox {
        GeoBox::new(Location::new(0.0, 0.0), Location::new(0.0, 0.0)) // TODO
    }
    
    fn gather_hotspots(&self, area: &GeoBox, hotspot_set: &mut HashSet<Hotspot>) { 
        // TODO
    }

    fn get_color_name(&self, atlas: &Atlas) -> ColorName {
        atlas.default_route_color.clone() // TODO
    }
}

// ---- test --------------------------------------------------------------------------------------

#[test]
fn test_path() {
}

