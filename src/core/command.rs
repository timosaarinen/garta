// Garta - A geography application for GTK3
// Copyright (C) 2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::fmt;

use core::*;
use geocoord::geo::{Location};

/// Command for undo/redo.
#[derive(Clone)]
pub enum Command {
    CreateAttraction{ 
        new: Attraction,
    },
    MoveAttraction{ 
        id: UniqueId,
        old_location: Location,
        new_location: Location,
    },
    UpdateAttraction{ 
        old: Attraction,
        new: Attraction,
    },
    DeleteAttraction{ 
        old: Attraction,
    },
}

impl fmt::Debug for Command {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Command::CreateAttraction{new} => {
                write!(f, "CreateAttraction {}", new.id())
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                write!(f, "MoveAttraction: {:.1}° {:.0}m",
                          old_location.bearing_to(new_location),
                          old_location.distance_to(new_location) )
            }
            Command::UpdateAttraction{old, new} => {
                write!(f, "UpdateAttraction {}", new.id())
            },
            Command::DeleteAttraction{old} => {
                write!(f, "DeleteAttraction {}", old.id())
            },
        }
    }
}

impl Command {
    /// Make DeleteAttraction command.
    pub fn make_delete_attraction(id: UniqueId, atlas: &Atlas) -> Option<Command> {
        if let Some(attr) = atlas.attractions.get(&id) {
            Some(Command::DeleteAttraction{old: attr.clone()})
        } else {
            warn!("attraction not found for id {}", id);
            None
        }
    }

    // Get a short name for the command.
    pub fn name(&self) -> String {
        match self {
            Command::CreateAttraction{new} => {
                "new".into()
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                "move".into()
            }
            Command::UpdateAttraction{old, new} => {
                "modify".into()
            },
            Command::DeleteAttraction{old} => {
                "delete".into()
            },
        }
    }

    // True if the command doesn't change anything.
    pub fn is_zero(&self) -> bool {
        match self {
            Command::CreateAttraction{new} => {
                false
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                old_location == new_location
            }
            Command::UpdateAttraction{old, new} => {
                old == new
            },
            Command::DeleteAttraction{old} => {
                false
            },
        }
    }

    // (Re)do the command for atlas
    pub fn execute(&self, atlas: &mut Atlas) {
        match self {
            Command::CreateAttraction{new} => {
                atlas.attractions.insert(new.id(), new.clone());
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                if let Some(attr) = atlas.attractions.get_mut(id) {
                    attr.location = new_location.clone();
                }
            }
            Command::UpdateAttraction{old, new} => {
                atlas.attractions.insert(new.id(), new.clone());
            },
            Command::DeleteAttraction{old} => {
                atlas.attractions.remove(&old.id());
            },
        }
    }
    
    // Undo the command for atlas
    pub fn undo(&self, atlas: &mut Atlas) {
        match self {
            Command::CreateAttraction{new} => {
                atlas.attractions.remove(&new.id());
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                if let Some(attr) = atlas.attractions.get_mut(id) {
                    attr.location = old_location.clone();
                }
            }
            Command::UpdateAttraction{old, new} => {
                atlas.attractions.insert(old.id(), old.clone());
            },
            Command::DeleteAttraction{old} => {
                atlas.attractions.insert(old.id(), old.clone());
            },
        }
    }

    // Outline an area where the modifications are affected.
    pub fn visibility(&self, atlas: &Atlas) -> Visibility {
        match self {
            Command::CreateAttraction{new} => {
                Visibility::new()
                    .min_zoom(new.min_zoom_level)
                    .max_zoom(new.max_zoom_level)
                    .location(&new.location)
                    .layer_of(new as &dyn MapElement, &atlas)
            },
            Command::MoveAttraction{id, old_location, new_location} => {
                if let Some(attr) = atlas.attractions.get(id) {
                    Visibility::new()
                        .location(&new_location)
                        .location(&old_location)
                        .min_zoom(attr.min_zoom_level)
                        .max_zoom(attr.max_zoom_level)
                        .layer_of(attr as &dyn MapElement, &atlas)
                } else {
                    Visibility::new()
                }
            }
            Command::UpdateAttraction{old, new} => {
                Visibility::new()
                    .min_zoom(old.min_zoom_level)
                    .min_zoom(new.min_zoom_level)
                    .max_zoom(old.max_zoom_level)
                    .max_zoom(new.max_zoom_level)
                    .location(&old.location)
                    .location(&new.location)
                    .layer_of(old as &dyn MapElement, &atlas)
                    .layer_of(new as &dyn MapElement, &atlas)
            },
            Command::DeleteAttraction{old} => {
                Visibility::new()
                    .min_zoom(old.min_zoom_level)
                    .max_zoom(old.max_zoom_level)
                    .location(&old.location)
                    .layer_of(old as &dyn MapElement, &atlas)
            },
        }
    }
}

