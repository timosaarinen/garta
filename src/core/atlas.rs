// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate serde_json;

use std::vec::{Vec};
use std::collections::{BTreeSet, BTreeMap, HashMap, VecDeque};
use std::cmp::*;

use core::*;
use tiles::{TileSource};
use geocoord::{Projection, Location};

// MEMORY MANAGEMENT
//
// The core domain model memory management is based on hashmaps and 64-bit 
// unsigned integers as keys. This approach allows a relatively flexible 
// graph-like data structures without a need for carbage collection or reference 
// counting. The disadvantage is a risk of integer overflow and a possibility of 
// dangling indecies. Neither of them is a major problem, and a lifetime isn't  
// long enough to create 2^64 map elements manually.

// ---- Atlas --------------------------------------------------------------------------------------

/// The root object in the domain model. Its memory management is based on
/// hashmaps to simplify things.
pub struct Atlas {
    pub slug: Slug,
    pub name: String,
    
    /// The highest id in use.
    current_unique_id: UniqueId,
    
    /// Layers
    pub layers: BTreeMap<UniqueId, Layer>,
    
    /// Attractions
    pub attractions: HashMap<UniqueId, Attraction>,

    /// GPX waypoints
    pub waypoints: HashMap<UniqueId, Waypoint>,
    
    /// GPX routes
    pub routes: HashMap<UniqueId, Path>,
    
    /// GPX tracks
    pub tracks: HashMap<UniqueId, Path>,
    
    /// Areas.
    pub areas: HashMap<UniqueId, Area>,
    
    /// Collection of maps.
    pub maps: BTreeMap<Slug, Map>,
    
    /// Access tokens for maps.
    pub tokens: HashMap<Slug, MapToken>,

    /// Color palette for light backgrounds
    pub colors_for_light: HashMap<ColorName, Color>,

    /// Color palette for dark backgrounds
    pub colors_for_dark: HashMap<ColorName, Color>,
    
    /// Fallback attraction color.
    pub default_attraction_color: ColorName,

    /// Fallback route color.
    pub default_route_color: ColorName,

    /// Fallback track color.
    pub default_track_color: ColorName,

    /// ElementKinds for attractions
    pub attraction_kinds: HashMap<Slug, ElementKind>,

    /// Command history for undo/redo.
    pub undo_history: VecDeque<Command>,

    /// Command history for undo/redo.
    pub redo_history: VecDeque<Command>,
    
    /// Load/save progress.
    pub status: AtlasLoadStatus,
}

impl Atlas {
    /// Constructor.
    pub fn new(slug: Slug, name: String) -> Atlas {
        let (palette_for_light, palette_for_dark) = make_default_palette();
        Atlas{
            slug: slug,
            name: name,
            current_unique_id: ZERO_ID,
            layers: BTreeMap::new(),
            attractions: HashMap::new(),
            attraction_kinds: HashMap::new(),
            waypoints: HashMap::new(),
            tracks: HashMap::new(),
            routes: HashMap::new(),
            areas: HashMap::new(),
            maps: BTreeMap::new(),
            tokens: HashMap::new(),
            colors_for_light: palette_for_light,
            colors_for_dark: palette_for_dark,
            default_attraction_color: ColorName::Red{},
            default_route_color: ColorName::Blue{},
            default_track_color: ColorName::Red{},
            undo_history: VecDeque::new(),
            redo_history: VecDeque::new(),
            status: AtlasLoadStatus::new(),
        }    
    }

    /// Returns the next unique id in scope of this `Atlas`.
    pub fn next_id(&mut self) -> Result<UniqueId, &'static str> {
        increment_id(&mut self.current_unique_id)
    }

    /// Load atlas
    pub fn start_load(&mut self) {
        self.status.total = 0;
        self.status.loaded = 0;
        self.status.ready = false;
        // TODO: async load
        
        // Some test data
        let mut generic_kind = ElementKind::new("Landmark".into());
        generic_kind.fields.push(ElementField::TextField{
            slug: "text".into(), 
            title: "Text".into(),
            max_len: Some(6),
            regex: Some(r"\S+\S+".to_string()),
        });
        generic_kind.fields.push(ElementField::IntegerField{
            slug: "int".into(), 
            title: "Integer".into(),
            min_value: Some(1),
            max_value: Some(10),
            suffix: Some("m".into()),
        });
        generic_kind.fields.push(ElementField::FloatField{
            slug: "float".into(), 
            title: "Float".into(),
            decimals: 1,
            min_value: Some(5.0),
            max_value: Some(10.0),
            suffix: Some("kg".into()),
        });
        generic_kind.fields.push(ElementField::ComboField{
            slug: "combo".into(), 
            title: "Combo".into(),
            values: vec!["Watermelon".into(), "Grapefruit".into(), "Banana".into()],
        });
        generic_kind.fields.push(ElementField::CheckboxField{
            slug: "checkbox".into(), 
            title: "Checkbox".into(),
            values: vec!["Vicuña".into(), "Llama".into(), "Alpaca".into()],
        });
        generic_kind.fields.push(ElementField::DateField{
            slug: "date".into(), 
            title: "Date".into(),
        });
        generic_kind.fields.push(ElementField::MonthDayRangeField{
            slug: "range".into(), 
            title: "Range".into(),
        });
        generic_kind.fields.push(ElementField::AltitudeField{
        });
        let generic_slug = String::from("landmark");
        self.attraction_kinds.insert(generic_slug.clone().into(), generic_kind);
        
        let mountain_kind = ElementKind::new("Mountain".into());
        let mountain_slug = String::from("mountain");
        self.attraction_kinds.insert(mountain_slug.clone().into(), mountain_kind);

        let mut attr = Attraction::new(self.next_id().unwrap(), "Aconcagua".into(), Location::new(-32.653611, -70.011111));
        attr.location.elevation = Some(6960.0);
        attr.kind = generic_slug;
        self.attractions.insert(attr.id(), attr);
        
    }
    
    /// Save atlas
    pub fn start_save(&mut self) -> bool {
        self.status.total = 0;
        self.status.loaded = 0;
        self.status.ready = false;
        // TODO: async save
        false
    }

    /// Return the backdrop layer id.
    pub fn backdrop_layer_id(&self) -> Option<UniqueId> {
        for (layer_id, layer) in &self.layers {
            if layer.backdrop() {
                return Some(*layer_id);
            }
        }
        None
    }
    
    /// Set layer order value and ensure that the BTree is valid after the change.
    pub fn set_layer_order(&mut self, layer_id: UniqueId, order: u16) {
        if let Some(mut layer) = self.layers.remove(&layer_id) {
            layer.order = order;
            self.layers.insert(layer_id, layer);
        }
    }
    
    /// Set map name value and ensure that the BTree is valid after the change.
    pub fn set_map_name(&mut self, map_slug: String, name: String) {
        if let Some(mut map) = self.maps.remove(&map_slug) {
            map.name = name;
            self.maps.insert(map_slug, map);
        }
    }

    /// Execute the given command and add it to undo history.
    pub fn execute(&mut self, cmd: &Command) {
        self.redo_history.clear();
        if !cmd.is_zero() {
            cmd.execute(self);
            self.undo_history.push_back(cmd.clone());
            if self.undo_history.len() > settings_read().undo_history_size() {
                self.undo_history.pop_front();
            }
            debug!("atlas.execute: {:?}", cmd);
        } else {
            debug!("atlas.execute: {:?} is_zero={}", cmd, cmd.is_zero());
        }
    }
    
    /// Undo one command in undo history. Returns the command or None.
    pub fn undo(&mut self) -> Option<Command> {
        if let Some(cmd) = self.undo_history.pop_back() {
            debug!("atlas.undo: {:?}", cmd);
            cmd.undo(self);
            self.redo_history.push_back(cmd.clone());
            Some(cmd)
        } else {
            debug!("atlas.undo: None");
            None
        }
    }
    
    /// Redo one command in redo history. Returns the command or None.
    pub fn redo(&mut self) -> Option<Command> {
        if let Some(cmd) = self.redo_history.pop_back() {
            debug!("atlas.redo: {:?}", cmd);
            cmd.execute(self);
            self.undo_history.push_back(cmd.clone());
            Some(cmd)
        } else {
            debug!("atlas.redo: None");
            None
        }
    }

    // Return next undo command or None if history is empty.   
    pub fn peek_undo(&self) -> Option<Command> {
        if let Some(cmd) = self.undo_history.back() {
            Some(cmd.clone())
        } else {
            None
        }
    }    

    // Return next undo command or None if redo list is empty.   
    pub fn peek_redo(&self) -> Option<Command> {
        if let Some(cmd) = self.redo_history.back() {
            Some(cmd.clone())
        } else {
            None
        }
    }
}

// ---- AtlasLoadStatus ---------------------------------------------------------------------------

pub struct AtlasLoadStatus {
    pub total: i64,
    pub loaded: i64,
    pub ready: bool,
}

impl AtlasLoadStatus {
    pub fn new() -> AtlasLoadStatus {
        AtlasLoadStatus {
            total: 0,
            loaded: 0,
            ready: false,
        }
    }
}

// ---- Layer -------------------------------------------------------------------------------------

/// Layer in a atlas containing map elements.
#[derive(Serialize, Deserialize, Debug)]
pub struct Layer {
    /// Unique id.
    id: UniqueId,
    
    /// Map name.
    pub name: String,
    
    /// Order. The layer with the highest order are drawn the topmost. 
    /// Backdrop layer is expected to be zero.
    pub order: u16,
    
    /// In case of transparent map layers this is set to some, otherwise empty.
    /// Notice that the backdrop map layer is defined in MapView.
    #[serde(default)]
    pub map_slug: String,

    /// Map elements on the layer.
    #[serde(skip_serializing_if = "BTreeSet::is_empty")]
    #[serde(default)]
    pub element_ids: BTreeSet<UniqueId>,

    /// Remote layer uri.
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(default)]
    pub remote_uri: String,
    
    /// Maps remote ids to local ones.
    #[serde(skip_serializing)]
    #[serde(default)]
    pub remote_to_local_ids: BTreeMap<UniqueId, UniqueId>,
    
    /// Maps local ids to remote ones.
    #[serde(skip_serializing)]
    #[serde(default)]
    pub local_to_remote_ids: BTreeMap<UniqueId, UniqueId>,
}

impl Layer {
    /// Constructor to create an empty layer.
    pub fn new(id: UniqueId, name: String, order: u16) -> Layer {
        Layer{
            id: id,
            name: name,
            order: order,
            map_slug: "".into(),
            element_ids: BTreeSet::new(),
            remote_uri: "".into(),
            remote_to_local_ids: BTreeMap::new(),
            local_to_remote_ids: BTreeMap::new(),
        }    
    }

    /// Id getter.    
    pub fn id(&self) -> UniqueId { self.id }
    
    /// Returns true if this is a backdrop layer (order = 0).
    pub fn backdrop(&self) -> bool {
        (self.order == 0)
    }
    
    /// True if this is a remote layer.
    pub fn is_remote(&self) -> bool {
        !self.remote_uri.is_empty()
    }
}

impl Ord for Layer {
    fn cmp(&self, other: &Self) -> Ordering {
        self.order.cmp(&other.order)
    }
}

impl PartialOrd for Layer {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.order.partial_cmp(&other.order)
    }
}

impl PartialEq for Layer {
    fn eq(&self, other: &Self) -> bool {
        self.order.eq(&other.order)
    }
}

impl Eq for Layer {}

// ---- Map ---------------------------------------------------------------------------------------

/// Slippy map parameters.
#[derive(Serialize, Deserialize, Debug)]
pub struct Map {
    /// Unique identifier of the map
    #[serde(default)]
    pub slug: Slug,
    
    /// Map name for UI
    #[serde(default)]
    pub name: String,
    
    /// Tile width
    #[serde(default, rename = "tile-width")]
    pub tile_width: Option<i32>,
    
    /// Tile height
    #[serde(default, rename = "tile-height")]
    pub tile_height: Option<i32>,

    /// Maximum zoom level provided by the map
    #[serde(default = "default_max_zoom_level", rename = "max-zoom-level")]
    pub max_zoom_level: u8,
    
    /// True if the map is to be used as an overlay
    #[serde(default)]
    pub transparent: bool,

    /// The map colors are dark that should be taken into account when choosing text and symbol colors.
    #[serde(default)]
    pub dark: bool,
    
    /// Override server define tile expiration. The value defines number of days.
    #[serde(default)]
    pub expire_override: Option<u16>,

    /// Tile url templates
    #[serde(default, rename = "urls")]
    pub url_templates: Vec<String>,

    /// Token to be substituted in url templates. Either a token slug or a literal token.
    #[serde(default)]
    pub token: String,

    /// HTTP User-Agent header field value
    #[serde(default)]
    pub user_agent: Option<String>,
    
    /// HTTP Referer header field value
    #[serde(default)]
    pub referer: Option<String>,

    /// Copyright texts
    #[serde(default)]
    pub copyrights: Vec<MapCopyright>,
}

impl Map {
    /// Constructor.
    pub fn new(slug: String, name: String) -> Map {
        Map {
            slug: slug,
            name: "".into(),
            tile_width: None,
            tile_height: None,
            max_zoom_level: default_max_zoom_level(),
            transparent: false,
            dark: false,
            expire_override: None,
            url_templates: Vec::new(),
            token: "".into(),
            user_agent: None,
            referer: None,
            copyrights: Vec::new(),
        }
    }
    
    /// Convert Map into a TileSource. It's required that tile width and height are available,
    /// and None will be returned if not.
    pub fn to_tile_source(&self, tokens: &HashMap<String, MapToken>) -> Option<TileSource> {
        // Interpret the token field either as a reference or a literal
        let token = {
            if let Some(token) = tokens.get(&self.token) {
                token.value.clone()
            } else {
                self.token.clone()
            }
        };

        // Build tile source
        if self.tile_width.is_some() && self.tile_height.is_some() {
            Some(TileSource {
                slug: self.slug.clone(),
                url_templates: self.url_templates.clone(),
                token: token,
                user_agent: self.user_agent.clone(),
                referer: self.referer.clone(),
                expire_override: self.expire_override.clone(),
                tile_width: self.tile_width.unwrap(),
                tile_height: self.tile_height.unwrap(),
            })
        } else {
            None
        }
    }
    
    /// Returns projection of the map.
    pub fn as_projection(&self) -> Projection {
        // Currently the only supported projection is Mercator one.
        Projection::new_mercator_projection()
    }
}

impl Ord for Map {
    // Name-based sorting.
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Map {
    // Name-based sorting.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl PartialEq for Map {
    // Name-based sorting.
    fn eq(&self, other: &Self) -> bool {
        self.name.eq(&other.name)
    }
}

impl Eq for Map {}

/// Map copyright information.
#[derive(Serialize, Deserialize, Debug)]
pub struct MapCopyright {
    pub text: String,
    pub url: String,
}

// ---- MapToken ----------------------------------------------------------------------------------
#[derive(Serialize, Deserialize, Debug)]
pub struct MapToken {
    pub value: String,
}

impl MapToken {
    pub fn new(value: String) -> MapToken {
        MapToken {
            value: value,
        }
    }
}

// ---- tests -------------------------------------------------------------------------------------

#[test]
fn test_atlas() {
    let la = Layer::new(1, "Nimi".into(), 0);
    // Create a atlas and layer
    let la_id = la.id();
    assert!(la.backdrop() == true);
    
    // Add the layer to the atlas
    let mut p = Atlas::new("atlas".into(), "Atlas".into());
    p.layers.insert(la.id(), la);
    
    assert!(p.backdrop_layer_id().unwrap() == la_id);
}

