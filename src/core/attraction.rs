// Garta - A geography application for GTK3
// Copyright (C) 2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::{HashSet, HashMap};

use core::{Atlas, UniqueId, Slug, MapElement, ElementFieldValue, ColorName, Hotspot, Command};
use geocoord::geo::{GeoBox, Location};

// ---- Attraction --------------------------------------------------------------------------------

/// A simple point-like destination on the map, also known as a landmark.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Attraction { 
    id: UniqueId,
    
    // Point-like location
    pub location: Location,
    
    /// Title of the attraction
    pub name: String,
    
    /// Color name, or None if ColorRule should be used.
    pub color: Option<ColorName>, 
    
    /// Slug for ElementKind.
    pub kind: Slug,
    
    /// Description formatted as MarkDown
    pub description: String,
    
    /// Picture url (remote or local)
    pub picture_uri: Option<String>,
    
    /// Element field values; the key is ElementField.slug.
    pub fields: HashMap<Slug, ElementFieldValue>,
    
    /// Minimum zoom level where the attraction is visible
    pub min_zoom_level: Option<u8>,

    /// Maximum zoom level where the attraction is visible
    pub max_zoom_level: Option<u8>,

    /// Tags related to the attraction
    pub tags: HashSet<String>,
    
    /// Ids of the layers this attraction belongs to.
    pub layer_ids: HashSet<UniqueId>,
}

impl Attraction {
    /// Constructor.
    pub fn new(id: UniqueId, name: String, loc: Location) -> Attraction {
        Attraction {
            id: id,
            location: loc,
            name: name,
            color: None,
            kind: "".into(),
            description: "".into(),
            picture_uri: None,
            fields: HashMap::new(),
            min_zoom_level: None,
            max_zoom_level: None,
            tags: HashSet::new(),
            layer_ids: HashSet::new(),
        }
    }

    /// Clone with a new id
    pub fn duplicate(&self, new_id: UniqueId) -> Attraction {
        let mut element = self.clone();
        element.id = new_id;
        element
    }
}

impl PartialEq for Attraction {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id &&
        self.location == other.location &&
        self.name == other.name &&
        self.color == other.color &&
        self.kind == other.kind &&
        self.description == other.description &&
        self.picture_uri == other.picture_uri &&
        {
            let mut r = self.fields.len() == other.fields.len();
            for (slug, v) in &self.fields {
                if let Some(ov) = other.fields.get(slug) {
                    if *v != *ov {
                        r = false;
                    }
                }
            }
            r
        } &&
        self.min_zoom_level == other.min_zoom_level &&
        self.max_zoom_level == other.max_zoom_level &&
        self.tags == other.tags &&
        self.layer_ids == other.layer_ids &&
        true
    }
}

impl MapElement for Attraction {
    fn id(&self) -> UniqueId { self.id }
    
    fn bounding_box(&self) -> GeoBox {
        GeoBox::new(self.location, self.location)
    }

    fn gather_hotspots(&self, area: &GeoBox, hotspot_set: &mut HashSet<Hotspot>) { 
        if area.contains(&self.location) {
            hotspot_set.insert(Hotspot::new(self.id, self.location, None));
        }
    }

    fn make_move_spot_command(&self, hotspot: &Hotspot) -> Option<Command> { 
        Some(Command::MoveAttraction{
            id: self.id,
            old_location: self.location,
            new_location: hotspot.location,
        })
    }
    
    fn is_deletable(&self, hotspot: &Hotspot) -> bool { true }
    
    fn make_delete_spot_command(&self, hotspot: &Hotspot) -> Option<Command> { 
        Some(Command::DeleteAttraction{
            old: self.clone(),
        })
    }
    
    fn is_visible_on_zoom_level(&self, zoom_level: u8) -> bool {
        if let Some(min_zoom_level) = self.min_zoom_level {
            if zoom_level < min_zoom_level {
                return false;
            }
        }
        if let Some(max_zoom_level) = self.max_zoom_level {
            if zoom_level > max_zoom_level {
                return false;
            }
        }
        return true;
    }

    fn get_color_name(&self, atlas: &Atlas) -> ColorName {
        let default_color_from_atlas = atlas.default_attraction_color.clone();
        if let Some(kind) = atlas.attraction_kinds.get(&self.kind) {
            let default_color = self.color.clone().unwrap_or(default_color_from_atlas);
            kind.get_color_name(&self.fields, default_color)
        } else {
            default_color_from_atlas
        }
    }
}

