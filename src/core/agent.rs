// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;

use std::collections::{HashMap, HashSet};

use self::chrono::{DateTime};
use self::chrono::prelude::{Utc};

use core::{Atlas, UniqueId, MapElement, Hotspot, ColorName};
use geocoord::geo::{GeoBox, Location, Vector};

// ---- Agent --------------------------------------------------------------------------------------

/// Agent is a moving element, typically a vehicle or a human. This is not directly related to
/// Vehicle struct.
#[derive(Clone, Debug)]
pub struct Agent { 
    id: UniqueId,
    
    /// Refers to the remote layer managing this agent.
    pub layer_id: UniqueId,
    
    /// Current location based on dead reckoning.
    pub estimated_location: Location,

    /// The latest reported location (without projection)
    pub reported_location: Location,
    
    /// Timestamp of the latest report
    pub reported_timestamp: DateTime<Utc>,

    /// Course over ground (CoG) in degrees (0°=north, 90°=east).
    pub bearing: f64,
    
    /// Speed over ground (SoG) in m/s.
    pub speed: f64,
    
    /// Rate of climb in m/s.
    pub vertical_speed: f64,

    /// Metadata about the agent and its current state.
    pub metadata: HashMap<&'static str, Metadata>,
}

impl Agent {
    /// Constructor.
    pub fn new(id: UniqueId, layer_id: UniqueId, loc: Location) -> Agent {
        Agent {        
            id: id,
            layer_id: layer_id,
            estimated_location: loc,
            reported_location: loc,
            reported_timestamp: Utc::now(),
            bearing: 0.0,
            speed: 0.0,
            vertical_speed: 0.0,
            metadata: HashMap::new(),
        }
    }
}

impl MapElement for Agent {
    fn id(&self) -> UniqueId { self.id }    

    fn bounding_box(&self) -> GeoBox {
        GeoBox::new(self.estimated_location, self.estimated_location)
    }
    
    fn gather_hotspots(&self, area: &GeoBox, hotspot_set: &mut HashSet<Hotspot>) { 
        if area.contains(&self.estimated_location) {
            hotspot_set.insert(Hotspot::new(self.id, self.estimated_location, None));
        }
    }

    fn get_color_name(&self, atlas: &Atlas) -> ColorName {
        ColorName::Red{} // TODO
    }
}

/// Agent metadata.
#[derive(Clone, Debug)]
pub enum Metadata {
  Boolean(bool),
  Integer(i64),
  Float(f64),
  Text(String),
  Vector(Vector),
  Location(Location),
}

