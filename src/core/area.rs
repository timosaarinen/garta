// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use core::{Atlas, UniqueId, MapElement, ColorName};
use geocoord::geo::{GeoBox, Location};

// ---- Area ---------------------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Area {
    id: UniqueId,
}

impl Area {
    pub fn new(id: UniqueId, slug: String) -> Area {
        Area{
            id: id,
        }    
    }
}

impl MapElement for Area {
    fn id(&self) -> UniqueId { self.id }
    
    fn bounding_box(&self) -> GeoBox {
        GeoBox::new(Location::new(0.0, 0.0), Location::new(0.0, 0.0)) // TODO
    }

    fn get_color_name(&self, atlas: &Atlas) -> ColorName {
        ColorName::Red{} // TODO
    }
}

// ---- test --------------------------------------------------------------------------------------

#[test]
fn test_area() {
}

