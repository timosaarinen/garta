// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub type UniqueId = u64;

pub const ZERO_ID: UniqueId = 0u64;

/// Increments the given id by one and then returns a copy of it. Returns None
/// if id can't be increased.
pub fn increment_id(id: &mut UniqueId) -> Result<UniqueId, &'static str> {
    if *id < u64::max_value() {
        *id += 1u64;
        Ok(*id)
    } else {
        Err("Reached maximum unique id")
    }
}

// ---- tests --------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_id() {
        let id1 = ZERO_ID;
        let mut id2 = id1;
        assert_eq!(id1, id2);
        increment_id(&mut id2).unwrap();
        assert_ne!(id1, id2);
    }
}
