// Garta - A geography application for GTK3
// Copyright (C) 2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;
//use self::chrono::prelude::*;

extern crate serde_json;

use std::collections::linked_list::LinkedList;
use std::collections::{BTreeSet};
use std::cmp::*;
use std::hash::{Hash, Hasher};
use std::path;

use geocoord::geo::{GeoBox, Location, Vector};
use core::{UniqueId, Atlas, MapElement, settings_read, serialize_to, deserialize_from};

// ---- MapView -----------------------------------------------------------------------------------

/// Metadata about map window to be stored when the app is stopped.
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct MapView {
    /// Outline of the view area.
    pub center: Location,

    /// The current focus location. This is affected by mouse pointed position.    
    #[serde(skip_serializing, default)]
    pub focus: Option<Location>,

    /// Zoom level of the view.
    pub zoom_level: u8,
    
    /// Visible layer ids.
    pub visible_layer_ids: LinkedList<UniqueId>,
    
    /// Backdrop layer map slug.
    pub map_slug: String,
    
    /// Coordinates format used within the view.
    pub coordinates_format: String,
    
    /// Chosen date and time for the map. `None` means the current system time.
    pub datetime: Option<chrono::DateTime<chrono::prelude::Utc>>,
    
    /// Window x and y position in pixels.
    pub window_position: Option<(i32, i32)>,
    
    /// Window width and height in pixels.
    pub window_size: Option<(i32, i32)>,
    
    /// Previous map slub (needed for tab cycling).
    #[serde(default)]
    pub prev_map_slug: Option<String>,
}

impl MapView {
    pub fn new() -> MapView {
        MapView {
            center: Location::new(0.0, 0.0),
            focus: None,
            zoom_level: 3,
            visible_layer_ids: LinkedList::new(),
            map_slug: "".into(),
            coordinates_format: "dm".into(),
            datetime: None,
            window_position: None,
            window_size: None,
            prev_map_slug: None,
        }
    }

    /// Save state to disk.
    pub fn store(&self) {
        // Write to cache dir
        let pathbuf = settings_read().mapview_file();
        match serialize_to(self, pathbuf) {
            Ok(()) => {
                debug!("Map view stored successfully: {:?}", self);
            },
            Err(e) => {
                warn!("Failed to store map view: {}", e);
            }
        }
    }

    /// Load state from disk.
    pub fn restore() -> MapView {
        // Read from cache dir
        let pathbuf = settings_read().mapview_file();
        if pathbuf.exists() {
            match deserialize_from::<MapView, path::PathBuf>(pathbuf) {
                Ok(map_view) => {
                    debug!("Map view restored successfully");
                    return map_view;
                },
                Err(e) => {
                    warn!("Failed to restore map view: {}", e);
                }
            }
        }
        MapView::new()
    }
}

// ---- Hotspot -----------------------------------------------------------------------------------

/// Clickable point of the element inside map view. Two spots are considered identical if their
/// id and refe are same.
#[derive(Clone, Debug)]
pub struct Hotspot {
    /// Owner element of the spot.
    pub id: UniqueId,
    
    /// Coordinates of the spot.
    pub location: Location,
    
    /// Internal reference within the element.
    pub refe: Option<i64>,

    /// Pixel position of the spot. This field is managed by GUI module.
    pub pos: Option<Vector>,
}

impl Hotspot {
    pub fn new(id: UniqueId, location: Location, refe: Option<i64>) -> Hotspot {
        Hotspot {
            id: id,
            location: location,
            refe: refe,
            pos: None,
        }
    }
}

impl PartialEq for Hotspot {
    fn eq(&self, other: &Hotspot) -> bool {
        self.id == other.id && self.refe == other.refe
    }
}

impl Eq for Hotspot {
}

impl Hash for Hotspot {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.refe.hash(state);
    }
}

// ---- Visibility --------------------------------------------------------------------------------

/// MapView related required visibility.
#[derive(Clone, Debug)]
pub struct Visibility {
    pub geobox: Option<GeoBox>,
    pub min_zoom_level: Option<u8>,
    pub max_zoom_level: Option<u8>,
    pub layer_ids: BTreeSet<UniqueId>,
}

impl Visibility {
    pub fn new() -> Visibility {
        Visibility {
            geobox: None,
            min_zoom_level: None,
            max_zoom_level: None,
            layer_ids: BTreeSet::new(),
        }
    }
    
    /// True if visible within the given parameters.
    pub fn visible_in(&self, geobox: &GeoBox, zoom_level: u8, visible_layer_ids: &LinkedList<UniqueId>) -> bool {
        if let Some(z) = self.min_zoom_level {
            if zoom_level < z {
                return false;
            }
        }
        if let Some(z) = self.max_zoom_level {
            if zoom_level > z {
                return false;
            }
        }
        if let Some(self_geobox) = self.geobox {
            if geobox.intersects(&self_geobox) {
                return false;
            }
        }
        
        for layer_id in &self.layer_ids {
            if visible_layer_ids.contains(&layer_id) {
                return true;
            }
        }
        return false;
    }
    
    /// Add min zoom level.
    pub fn min_zoom(&self, z: Option<u8>) -> Visibility {
        if let Some(z) = z {
            if let Some(cz) = self.min_zoom_level {
                Visibility {
                    geobox: self.geobox,
                    min_zoom_level: Some(min(cz, z)),
                    max_zoom_level: self.max_zoom_level,
                    layer_ids: self.layer_ids.clone(),
                }
            } else {
                Visibility {
                    geobox: self.geobox,
                    min_zoom_level: Some(z),
                    max_zoom_level: self.max_zoom_level,
                    layer_ids: self.layer_ids.clone(),
                }
            }
        } else {
            return self.clone();
        }
    }
    
    /// Add max zoom level.
    pub fn max_zoom(&self, z: Option<u8>) -> Visibility {
        if let Some(z) = z {
            if let Some(cz) = self.max_zoom_level {
                Visibility {
                    geobox: self.geobox,
                    min_zoom_level: self.min_zoom_level,
                    max_zoom_level: Some(max(cz, z)),
                    layer_ids: self.layer_ids.clone(),
                }
            } else {
                Visibility {
                    geobox: self.geobox,
                    min_zoom_level: self.min_zoom_level,
                    max_zoom_level: Some(z),
                    layer_ids: self.layer_ids.clone(),
                }
            }
        } else {
            return self.clone();
        }
    }
    
    // Add location.
    pub fn location(&self, loc: &Location) -> Visibility {
        if let Some(geobox) = self.geobox {
            Visibility {
                geobox: Some(geobox.expand(loc)),
                min_zoom_level: self.min_zoom_level,
                max_zoom_level: self.max_zoom_level,
                layer_ids: self.layer_ids.clone(),
            }
        } else {
            Visibility {
                geobox: Some(GeoBox::new(*loc, *loc)),
                min_zoom_level: self.min_zoom_level,
                max_zoom_level: self.max_zoom_level,
                layer_ids: self.layer_ids.clone(),
            }
        }
    }

    /// Add layer id for the given element id.
    pub fn layer_of<T: MapElement + ?Sized>(&self, elem: &T, atlas: &Atlas) -> Visibility {
        let mut l = self.layer_ids.clone();
        for (layer_id, layer) in &atlas.layers {
            if layer.element_ids.contains(&elem.id()) {
                l.insert(*layer_id);
            }
        }

        Visibility {
            geobox: self.geobox,
            min_zoom_level: self.min_zoom_level,
            max_zoom_level: self.max_zoom_level,
            layer_ids: l,
        }
    }
}

// ---- tests -------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use core::*;
    use geocoord::*;

    #[test]
    fn test_visibility() {
        //
        // Test empty
        //
        let mut v = Visibility::new();
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 99);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 99);
        assert_eq!(v.layer_ids.len(), 0);

        //
        // Test min zoom    
        //
        v = v.min_zoom(Some(8));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 8);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 99);
        
        v = v.min_zoom(Some(4));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 99);
        
        v = v.min_zoom(Some(6));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 99);

        // Test max zoom
        v = v.max_zoom(Some(10));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 10);

        v = v.max_zoom(Some(14));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 14);

        v = v.max_zoom(Some(12));
        assert!(v.geobox.is_none());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 14);

        //
        // Test geobox
        //
        let loc1 = Location::new_with_str("10°N 10°E").unwrap();
        let loc2 = Location::new_with_str("20°N 20°E").unwrap();
        let loc3 = Location::new_with_str("30°N 30°E").unwrap();
        
        v = v.location(&loc1);
        assert!(v.geobox.is_some());

        v = v.location(&loc3);
        assert!(v.geobox.is_some());
        assert!(v.geobox.unwrap().contains(&loc2));

        assert!(v.geobox.is_some());
        assert_eq!(v.min_zoom_level.unwrap_or(99), 4);
        assert_eq!(v.max_zoom_level.unwrap_or(99), 14);

        //
        // Test layers    
        //
        let mut atlas = Atlas::new("atlas".into(), "Atlas".into());
        let mut layer1 = Layer::new(atlas.next_id().unwrap(), "layer1".into(), 1);
        let mut layer2 = Layer::new(atlas.next_id().unwrap(), "layer2".into(), 2);
        let layer3 = Layer::new(atlas.next_id().unwrap(), "layer3".into(), 3);
        let a1 = Attraction::new(atlas.next_id().unwrap(), "a1".into(), loc1);
        let a2 = Attraction::new(atlas.next_id().unwrap(), "a2".into(), loc1);
        layer1.element_ids.insert(a1.id());
        layer2.element_ids.insert(a1.id());
        layer2.element_ids.insert(a2.id());
        let layer1_id = layer1.id();
        let layer2_id = layer2.id();
        let layer3_id = layer3.id();
        atlas.layers.insert(layer1.id(), layer1);
        atlas.layers.insert(layer2.id(), layer2);
        atlas.layers.insert(layer3.id(), layer3);
        
        v = v.layer_of(&a1, &atlas);
        v = v.layer_of(&a2, &atlas);
        assert!(v.layer_ids.contains(&layer1_id));
        assert!(v.layer_ids.contains(&layer2_id));
        assert!(!v.layer_ids.contains(&layer3_id));
        assert_eq!(v.layer_ids.len(), 2);
    }
}

