// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;
use self::chrono::prelude::*;

extern crate regex;
use self::regex::{Regex};

use std::collections::{HashSet, HashMap};

use core::{Atlas, UniqueId, Slug, Hotspot, Command, ColorRule, ColorName};
use geocoord::geo::{GeoBox};
use core::persistence::{serialize_naivedate, deserialize_naivedate};

// ---- MapElement ---------------------------------------------------------------------------------

pub trait MapElement {
    /// Returns the unique id of the element
    fn id(&self) -> UniqueId;

    /// Returns bounding box of the element.
    fn bounding_box(&self) -> GeoBox;

    /// True if this is an element from a layer persisted remotely.
    fn is_remote(&self) -> bool { false }
    
    /// Add hotspots of the element inside the given area to the given set.
    fn gather_hotspots(&self, area: &GeoBox, hotspot_set: &mut HashSet<Hotspot>) { }

    /// Requests a command to be used to move the element to a new location. The wished location
    /// is read from `hotspot.location` field. Returns `None` if moving isn't possible.
    fn make_move_spot_command(&self, hotspot: &Hotspot) -> Option<Command> { None }
    
    /// True if the spot can be deleted.
    fn is_deletable(&self, hotspot: &Hotspot) -> bool { false }
    
    /// Requests a command to be used to delete the spot. Returns None if deleting isn't possible.
    fn make_delete_spot_command(&self, hotspot: &Hotspot) -> Option<Command> { None }
    
    /// True if the element is visible on the given zoom level
    fn is_visible_on_zoom_level(&self, zoom_level: u8) -> bool { true }
    
    /// Check the color rule and return a color name based on that.
    fn get_color_name(&self, atlas: &Atlas) -> ColorName;
}

// ---- ElementKind --------------------------------------------------------------------------------

/// Element kind/type definition.
pub struct ElementKind { 
    /// Title of the kind
    pub name: String,

    /// Icon file basename
    pub icon_uri: Option<String>,
    
    /// Description
    pub description: String,

    /// Color rule which is used to determine attraction color.
    pub color_rule: ColorRule,
    
    /// Fields
    pub fields: Vec<ElementField>,
    
    /// Content of the second title line of attraction
    pub second_line: Option<ElementSubtitle>,
}

impl ElementKind {
    /// Constructor.
    pub fn new(name: String) -> ElementKind {
        ElementKind {
            name: name,
            icon_uri: None,
            description: String::from(""),
            color_rule: ColorRule::UserDefineable{},
            fields: Vec::new(),
            second_line: None,
        }
    }

    pub fn get_color_name(&self, fields: &HashMap<String, ElementFieldValue>, default_color: ColorName) -> ColorName {
        match self.color_rule {
            ColorRule::UserDefineable{} => {
                default_color
            },
            ColorRule::Fixed{ref color} => {
                color.clone()
            },
            // TODO
            _ => {
                default_color
            }
        }
    }
}

// ---- ElementField -------------------------------------------------------------------------------

pub type ElementFieldSlug = String;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ElementField {
    TextField{
        slug: Slug,
        title: String,
        max_len: Option<u32>,
        regex: Option<String>,
    },
    IntegerField{
        slug: Slug,
        title: String,
        min_value: Option<i64>,
        max_value: Option<i64>,
        suffix: Option<String>,
    },
    FloatField{
        slug: Slug,
        title: String,
        decimals: u8,
        min_value: Option<f64>,
        max_value: Option<f64>,
        suffix: Option<String>,
    },
    ComboField{
        slug: Slug,
        title: String,
        values: Vec<String>,
    },
    CheckboxField{
        slug: Slug,
        title: String,
        values: Vec<String>,
    },
    DateField{
        slug: Slug,
        title: String,
    },
    MonthDayRangeField{
        slug: Slug,
        title: String,
    },
    AltitudeField{
    },
}

impl ElementField {
    /// True if the given value is within expected range, false otherwise.
    pub fn valid(&self, value: &ElementFieldValue) -> bool {
        match self {
            ElementField::TextField{ slug, title, max_len, regex } => {
                match value {
                    ElementFieldValue::Text{ value } => {
                        if let Some(re_s) = regex {
                            match Regex::new(re_s) {
                                Ok(re) => {
                                    re.is_match(value)
                                },
                                Err(e) => {
                                    warn!("Syntax error in regular expression: {}", re_s);
                                    false
                                }
                            }
                        } else {
                            true
                        }
                    },
                    _ => {
                        false
                    }
                }
            },
            ElementField::IntegerField{ slug, title, min_value, max_value, suffix } => {
                match value {
                    ElementFieldValue::Integer{ value } => {
                        if let Some(minv) = min_value {
                            if let Some(maxv) = max_value {
                                minv <= value && value <= maxv
                            } else {
                                minv <= value
                            }
                        } else {
                            if let Some(maxv) = max_value {
                                value <= maxv
                            } else {
                                true
                            }
                        }
                    },
                    _ => {
                        false
                    }
                }
            },
            ElementField::FloatField{ slug, title, decimals, min_value, max_value, suffix } => {
                match value {
                    ElementFieldValue::Decimal{ value } => {
                        if let Some(minv) = min_value {
                            if let Some(maxv) = max_value {
                                minv <= value && value <= maxv
                            } else {
                                minv <= value
                            }
                        } else {
                            if let Some(maxv) = max_value {
                                value <= maxv
                            } else {
                                true
                            }
                        }
                    },
                    _ => {
                        false
                    }
                }
            },
            ElementField::ComboField{ slug, title, values } => {
                match value {
                    ElementFieldValue::Text{ value } => {
                        values.contains(value)
                    },
                    _ => {
                        false
                    }
                }
            },
            ElementField::CheckboxField{ slug, title, values } => {
                match value {
                    ElementFieldValue::MultiText{ mvalue } => {
                        let mut r = true;
                        for v in mvalue {
                            r = r && values.contains(v);
                        }
                        r
                    },
                    _ => {
                        false
                    }
                }
            },
            ElementField::DateField{ slug, title } => {
                true
            },
            ElementField::MonthDayRangeField{ slug, title } => {
                true
            },
            ElementField::AltitudeField{ } => {
                match value {
                    ElementFieldValue::Decimal{ value } => {
                        -6357000.0 <= *value
                    },
                    _ => {
                        false
                    }
                }
            },
        }
    }
}

// ---- ElementFieldValue --------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ElementFieldValue {
    Text{ value: String },
    MultiText{ mvalue: HashSet<String> },
    Boolean{ value: bool},
    Integer{ value: i64},
    Decimal{ value: f64},
    Date{ 
        #[serde(serialize_with = "serialize_naivedate", deserialize_with = "deserialize_naivedate")]
        value: NaiveDate
    },
    MonthDay{ 
        month_from: Option<u8>,
        day_from: Option<u8>,
        month_to: Option<u8>,
        day_to: Option<u8>,
    },
}

impl ElementFieldValue {
    /// Tests if the given datetime is within the range defined in MonthDay.
    /// Returns None if the ElementFieldValue isn't a `MonthDay`.
    fn contains(&self, datetime: chrono::DateTime<chrono::prelude::Utc>) -> Option<bool> {
        let m = datetime.month() as u16 * 32 + datetime.day() as u16;
        match self {
            ElementFieldValue::MonthDay{month_from, day_from, month_to, day_to} => {
                // From day
                let f = {
                    if let Some(mf) = month_from {
                        if let Some(df) = day_from {
                            32 * (*mf as u16) + *df as u16
                        } else {
                            32 * (*mf as u16) + 31
                        }
                    } else {
                        0
                    }
                };
            
                // To day
                let t = {
                    if let Some(mt) = month_to {
                        if let Some(dt) = day_to {
                            32 * *mt as u16 + *dt as u16
                        } else {
                            32 * *mt as u16 + 31
                        }
                    } else {
                        32 * 13
                    }
                };
                
                // The final decision
                if f <= t {
                    // Range not covering New Year
                    Some(f <= m && m <= t)
                } else {
                    // Range over New Year
                    Some(f <= m || m <= t)
                }
            },
            _ => {
                None
            }
        }
    }
}

impl PartialEq for ElementFieldValue {
    fn eq(&self, other: &Self) -> bool {
        match self {
            ElementFieldValue::Text{ value } => {
                let svalue = value;
                match other {
                    ElementFieldValue::Text{ value } => { svalue == value }, _ => { false }
                }
            },
            ElementFieldValue::MultiText{ mvalue } => {
                let smvalue = mvalue;
                match other {
                    ElementFieldValue::MultiText{ mvalue } => { smvalue == mvalue }, _ => { false }
                }
            },
            ElementFieldValue::Boolean{ value } => {
                let svalue = value;
                match other {
                    ElementFieldValue::Boolean{ value } => { svalue == value }, _ => { false }
                }
            },
            ElementFieldValue::Integer{ value } => {
                let svalue = value;
                match other {
                    ElementFieldValue::Integer{ value } => { svalue == value }, _ => { false }
                }
            },
            ElementFieldValue::Decimal{ value } => {
                let svalue = value;
                match other {
                    ElementFieldValue::Decimal{ value } => { svalue == value }, _ => { false }
                }
            },
            ElementFieldValue::Date{ value } => {
                let svalue = value;
                match other {
                    ElementFieldValue::Date{ value } => { svalue == value }, _ => { false }
                }
            },
            ElementFieldValue::MonthDay{ month_from, day_from, month_to, day_to } => {
                let smonth_from = month_from;
                let sday_from = day_from;
                let smonth_to = month_to;
                let sday_to = day_to;
                match other {
                    ElementFieldValue::MonthDay{ month_from, day_from, month_to, day_to } => { 
                        smonth_from == month_from &&
                        sday_from == day_from &&
                        smonth_to == month_to &&
                        sday_to == day_to
                    }, 
                    _ => { false }
                }
            },
        }
    }
}

// ---- ElementSubtitle ----------------------------------------------------------------------------

/// Defines content of the second title line of attraction.
pub enum ElementSubtitle {
    Altitude,
    Coordinates{
        /// Text representation of the coordinates: 
        /// "d"   = decrees (40.0°S 25.0°E)
        /// "dm"  = decrees and minutes (40°0.0'S 25°0.0'E)
        /// "dms" = decrees, minutes and seconds (40°0'0.0"S 25°0'0.0"E) 
        /// "-d"  = decrees with sign (-40.0)
        format: String,
        decimals: u8,
    },
    Field{ title: String },
    Fixed{ text: String },
}

// ---- test ---------------------------------------------------------------------------------------

#[test]
fn test_path() {
}

