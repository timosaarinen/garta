// Garta - A geography application for GTK3
// Copyright (C) 2016-2019, Timo Saarinen
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate serde_json;

use std::collections::{HashMap};

// ---- Color --------------------------------------------------------------------------------------

/// RGBA color.
#[derive(Clone, Copy, Serialize, Deserialize, PartialEq, Debug)]
pub struct Color {
    pub red: f64,
    pub green: f64,
    pub blue: f64,
    pub alpha: f64,
}

impl Color {
    /// Construct a new color with alpha.
    pub fn new(r: f64, g: f64, b: f64, a: f64) -> Color {
        Color {
            red: r,
            green: g,
            blue: b,
            alpha: a,
        }
    }
    
    /// Construct a new color without alpha.
    pub fn opaque(r: f64, g: f64, b: f64) -> Color {
        Color {
            red: r,
            green: g,
            blue: b,
            alpha: 0.0,
        }
    }

    /// Constructor for RGB tuple.
    pub fn with_tuple(rgb: (f64, f64, f64)) -> Color {
        Color {
            red: rgb.0,
            green: rgb.1,
            blue: rgb.2,
            alpha: 0.0,
        }
    }
    
    /// Construct a black color.
    pub fn black() -> Color {
        Color::opaque(0.0, 0.0, 0.0)
    }
    
    /// Construct a black color.
    pub fn white() -> Color {
        Color::opaque(1.0, 1.0, 1.0)
    }
    
    /// Return color with alpha set to given value.
    pub fn alpha(&self, alpha: f64) -> Color {
        Color {
            red: self.red,
            green: self.green,
            blue: self.blue,
            alpha: alpha,
        }
    }
    
    /// Weighted average of two colors. 0.0 results self, 1.0 results the other and 0.5 average.
    #[inline]
    pub fn blend_with(&self, other: Color, weight: f64) -> Color {
        let w1 = 1.0 - weight;
        Color {
            red:   w1 * self.red   + weight * other.red,
            green: w1 * self.green + weight * other.green,
            blue:  w1 * self.blue  + weight * other.blue,
            alpha: w1 * self.alpha + weight * other.alpha,
        }
    }
    
    /// Square of distance to the other color. Alpha is ignored.
    #[inline]
    pub fn distance_to(&self, other: Color) -> f64 {
        let sq = |x| { x * x};
        
        (sq(self.red - other.red) + sq(self.green - other.green) + sq(self.blue - other.blue)).sqrt()
    }
    
    /// Returns RGBA tuple of the color.
    pub fn to_rgba(&self) -> (f64, f64, f64, f64) {
        (self.red, self.green, self.blue, self.alpha)
    }
}

// ---- ColorName ----------------------------------------------------------------------------------

#[derive(Serialize, Deserialize, Clone, Hash, Eq, PartialEq, Debug)]
pub enum ColorName {
    Magenta,
    Red,
    Orange,
    Yellow,
    Lime,
    Green,
    Cyan,
    Blue,
    Violet,
    Brown,
    Gray,
}

/// Sorted array or color names.
pub const COLOR_NAMES: [ColorName; 11] = [
    ColorName::Magenta,
    ColorName::Red,
    ColorName::Orange,
    ColorName::Yellow,
    ColorName::Lime,
    ColorName::Green,
    ColorName::Cyan,
    ColorName::Blue,
    ColorName::Violet,
    ColorName::Brown,
    ColorName::Gray,
];

impl std::string::ToString for ColorName {
    fn to_string(&self) -> String {
        match self {
            ColorName::Magenta => { return "magenta".into() },
            ColorName::Red =>     { return "red".into() },
            ColorName::Orange =>  { return "orange".into() },
            ColorName::Yellow =>  { return "yellow".into() },
            ColorName::Lime =>    { return "lime".into() },
            ColorName::Green =>   { return "green".into() },
            ColorName::Cyan =>    { return "cyan".into() },
            ColorName::Blue =>    { return "blue".into() },
            ColorName::Violet =>  { return "violet".into() },
            ColorName::Brown =>   { return "brown".into() },
            ColorName::Gray =>    { return "gray".into() },
        }
    }
}

impl std::convert::TryFrom<String> for ColorName {
    type Error = String;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        match s.to_lowercase().as_ref() {
            "magenta" => Ok(ColorName::Magenta),
            "red" => Ok(ColorName::Red),
            "orange" => Ok(ColorName::Orange),
            "yellow" => Ok(ColorName::Yellow),
            "lime" => Ok(ColorName::Lime),
            "green" => Ok(ColorName::Green),
            "cyan" => Ok(ColorName::Cyan),
            "blue" => Ok(ColorName::Blue),
            "violet" => Ok(ColorName::Violet),
            "brown" => Ok(ColorName::Brown),
            "gray" => Ok(ColorName::Gray),
            _ => Err(format!("Unknown color name: {}", s)),
        }
    }
}

impl std::convert::TryFrom<&str> for ColorName {
    type Error = String;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        match String::from(s).to_lowercase().as_ref() {
            "magenta" => Ok(ColorName::Magenta),
            "red" => Ok(ColorName::Red),
            "orange" => Ok(ColorName::Orange),
            "yellow" => Ok(ColorName::Yellow),
            "lime" => Ok(ColorName::Lime),
            "green" => Ok(ColorName::Green),
            "cyan" => Ok(ColorName::Cyan),
            "blue" => Ok(ColorName::Blue),
            "violet" => Ok(ColorName::Violet),
            "brown" => Ok(ColorName::Brown),
            "gray" => Ok(ColorName::Gray),
            _ => Err(format!("Unknown color name: {}", s)),
        }
    }
}

// ---- ColorRule ----------------------------------------------------------------------------------

/// A rule how to color map element.
#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug)]
pub enum ColorRule {
    UserDefineable {
    },
    Fixed {
        color: ColorName,
    },
    BooleanField {
        true_color: ColorName,
        false_color: ColorName,
    },
    DecimalField {
        color_range: Vec<DecimalColorPoint>,
    },
    DateRangeField {
        in_color: ColorName,
        out_color: ColorName,
    },
}

/// A combination of f64 and ColorName. This has to be defined because there is no Eq for f64.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DecimalColorPoint {
    value: f64,
    color: ColorName,
}

impl PartialEq for DecimalColorPoint {
    fn eq(&self, other: &DecimalColorPoint) -> bool {
        self.value == other.value && self.color == other.color
    }
}
impl Eq for DecimalColorPoint {}

// ---- palette -----------------------------------------------------------------------------------

/// Build the default color palettes for Atlas. The first one is for light backgrounds, the second 
/// one for dark ones.
pub fn make_default_palette() -> (HashMap<ColorName, Color>, HashMap<ColorName, Color>) {
    let mut c4l = HashMap::new();
    let mut c4d = HashMap::new();

    c4l.insert(ColorName::Brown,   Color::opaque(0.75, 0.375, 0.0));
    c4d.insert(ColorName::Brown,   Color::opaque(0.5,  0.25,  0.0));

    c4l.insert(ColorName::Magenta, Color::opaque(0.85, 0.0,   0.85));
    c4d.insert(ColorName::Magenta, Color::opaque(1.0,  0.0,   1.0));

    c4l.insert(ColorName::Red,     Color::opaque(1.0,  0.0,   0.0));
    c4d.insert(ColorName::Red,     Color::opaque(1.0,  0.5,   0.5));

    c4l.insert(ColorName::Orange,  Color::opaque(1.0,  0.5,   0.0));
    c4d.insert(ColorName::Orange,  Color::opaque(1.0,  0.75,  0.5));

    c4l.insert(ColorName::Yellow,  Color::opaque(0.8,  0.8,   0.0));
    c4d.insert(ColorName::Yellow,  Color::opaque(1.0,  1.0,   0.5));

    c4l.insert(ColorName::Lime,    Color::opaque(0.4,  0.75,  0.0));
    c4d.insert(ColorName::Lime,    Color::opaque(0.75, 1.0,   0.5));

    c4l.insert(ColorName::Green,   Color::opaque(0.0,  0.7,   0.0));
    c4d.insert(ColorName::Green,   Color::opaque(0.5,  1.0,   0.5));

    c4l.insert(ColorName::Cyan,    Color::opaque(0.0,  0.7,   0.7));
    c4d.insert(ColorName::Cyan,    Color::opaque(0.0,  1.0,   1.0));

    c4l.insert(ColorName::Blue,    Color::opaque(0.0,  0.0,   1.0));
    c4d.insert(ColorName::Blue,    Color::opaque(0.5,  0.5,   1.0));

    c4l.insert(ColorName::Violet,  Color::opaque(0.5,  0.0,   1.0));
    c4d.insert(ColorName::Violet,  Color::opaque(0.5,  0.0,   1.0));

    c4l.insert(ColorName::Gray,    Color::opaque(0.5,  0.5,   0.5));
    c4d.insert(ColorName::Gray,    Color::opaque(0.8,  0.8,   0.8));
    
    (c4l, c4d)
}

// ---- tests --------------------------------------------------------------------------------------

#[test]
fn test_color() {
    let black = Color::black();
    let white = Color::white();
    
    // Distance between black and white
    assert_eq!(black.distance_to(white), 3.0f64.sqrt());
    
    // Mix black and white to get gray    
    let gray = black.blend_with(white, 0.5);
    assert_eq!(gray.red, 0.5);
    assert_eq!(gray.green, 0.5);
    assert_eq!(gray.blue, 0.5);
    
    // Mix black and with with weights 0.0 and 1.0
    assert_eq!(black.blend_with(white, 0.0), black);
    assert_eq!(black.blend_with(white, 1.0), white);
    
    // Alpha
    let orange = Color::opaque(1.0, 0.6, 0.0);
    let orange_a = orange.alpha(0.8);
    assert_eq!(orange_a.red, 1.0);
    assert_eq!(orange_a.green, 0.6);
    assert_eq!(orange_a.blue, 0.0);
    assert_eq!(orange_a.alpha, 0.8);
    
    // PartialEq
    assert_eq!(orange, orange_a.alpha(0.0));
}

#[test]
fn test_palette() {
    let (p4l, p4d) = make_default_palette();
    let c4l = p4l.get(&ColorName::Orange{}).unwrap();
    let c4d = p4d.get(&ColorName::Orange{}).unwrap();
    
    assert_eq!(c4l.red,   1.0);
    assert_eq!(c4l.green, 0.5);
    assert_eq!(c4l.blue,  0.0);
    
    assert_eq!(c4d.red,   1.0);
    assert_eq!(c4d.green, 0.75);
    assert_eq!(c4d.blue,  0.5);
}

